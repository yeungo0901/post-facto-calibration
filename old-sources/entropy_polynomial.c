#include <stdio.h>
#include <math.h>
#include <stdlib.h>


double function_poly(double x);
double rand_in_bounds(double min, double max);
double calc_entropy(double xi, double epsilon);

int main (int argc, char *argv[]){

  srandom(123456);
  double xi_initial = -1.0;
  double eps_list[] = {0.01, 0.1, 0.5, 1.0, 2.0, 5.0, 10.0};
  int n_epsilons = sizeof(eps_list)/sizeof(eps_list[0]);
  int epsilon_ind, n_steps_ind;
  double epsilon;
  int n_steps = 20000;
  double se, fitness_next;
  printf("# n_step xi fitness se epsilon \n");

  for(epsilon_ind = 0;epsilon_ind<n_epsilons;epsilon_ind++){
    epsilon = eps_list[epsilon_ind];
    printf("#epsilon_%g'\n", epsilon);
    printf("# epsilon %g \n", epsilon); 
      /*double epsilon = 0.2;*/
    double xi = xi_initial;
    double xi_next = xi_initial;
    double fitness = function_poly(xi_initial);

    for(n_steps_ind = 0; n_steps_ind < n_steps; n_steps_ind++){
      xi_next = xi + rand_in_bounds(-epsilon,epsilon);
      fitness_next = function_poly(xi_next);  
      if(fitness_next > fitness) {
	se = calc_entropy(xi, epsilon);
	/*printf("%d %g %g %g %g\n", n_steps_ind, xi, fitness, 0, epsilon);*/
	fitness = fitness_next; 
	xi = xi_next;
      }
      printf("%d %g %g %g %g\n", n_steps_ind, xi, fitness, se, epsilon);
    }
    printf("\n\n");
      } 
  return 0;
}

double function_poly(double x){
  return -(x+3.0)*(x-7.0)*(x+9.0)*(x-8.0)*(x-20.0)*(x-2.0);
}

double rand_in_bounds(double min, double max){
  return min + ((max-min)*(double)random())/RAND_MAX;
}

double calc_entropy(double xi, double epsilon){
  int x_yes, x_no;
  double fitness = function_poly(xi);
  double prob, se;
  x_yes = 0;
  x_no = 0;
  int x_ind;
  int n_entropy_tries = 10000;

  for(x_ind=0;x_ind < n_entropy_tries; x_ind++){
    /* this is the algorithm step */
    double xi_try = xi + rand_in_bounds(-epsilon,epsilon);
    if (function_poly(xi_try) > fitness)
      {
	x_yes++;
      }
    else {
      x_no++;
    }
  }         
  prob = (1.0*x_yes)/(1.0*n_entropy_tries);
  if(x_yes == 0)
    {
      se = 0.0;
    }
  else{
    se = -prob*log(prob);
  }
  return se;
  
}
