#include "scurve-types.h"
#include "prototypes.h"

Source_type source;
double step_size, noise_percentage;
Poly scurve_true, scurve_initial;
int n_steps, real_shift;
int nr_computations;


int main(int argc, char *argv[])
{
  char output_dir[MAX_FILENAME_LEN];
  char fitness_filepath[MAX_FILENAME_LEN], fitness_filepath_only_increases[MAX_FILENAME_LEN];  
  double *y_readout = NULL;
  char run_id_str_eps[MAX_FILENAME_LEN];
  setting_params(argc, argv, adaptive);  

  if (is_finite(source)) {    
    sprintf(run_id_str_eps, "eps%1.5g_srcY%1.5g_Z%1.5g", step_size, source.y
	    ,source.z);
    	
  } else {
    sprintf(run_id_str_eps, "eps%1.5g_srcA%1.5g", step_size, source.a);
  }
  make_directories(output_dir);  
  sprintf(fitness_filepath, "%s/%s_%s_%s", 
	  output_dir, "adaptive", run_id_str_eps, "fit_timeline");
  sprintf(fitness_filepath_only_increases, "%s/%s_%s_%s", 
	  output_dir, "adaptive", run_id_str_eps, "fit_timeline_incr");
  print_metadata(fitness_filepath);
  print_state_header(fitness_filepath);
  prepare_mask_and_readout(&y_readout, generate);
  add_noise(noise_percentage, y_readout);

  int step;
  Poly scurve = scurve_initial;
  double fitness = calc_fitness(scurve, y_readout);
  Poly pnormal, plarge;
  double step_size_large;
  double entropy = shannon_entropy(adaptive, scurve_initial, y_readout);
  double step_size_adaptive = step_size;

  double metric;
  for (step = 0; step < n_steps; ++step) {
    adapt_prep_step(scurve, &pnormal, &plarge, step, step_size_adaptive,
		    &step_size_large); 
    fitness = calc_fitness(scurve, y_readout);
    double fitness_pnormal = calc_fitness(pnormal, y_readout);
    double fitness_plarge = calc_fitness(plarge, y_readout);

    /* now the subtle conditions on fitness to say: (1) do we take a
       step, and (2) which is it: the normal or the large? */
    static int count_since_last_improvement = 0;
    if (fitness_pnormal > fitness || fitness_plarge > fitness) {
      /* an improvement */
      if (fitness_plarge > fitness_pnormal) 
	{
	  metric = metric_coeffs(scurve, plarge);
	  scurve = plarge;
	  fitness = fitness_plarge; /* (do we need it?) */

	  /* this update means we broaden our search for a while */
	  step_size_adaptive = step_size_large;
	  update_c0(scurve, source, y_readout);
	} 
      else 
	{
	  metric = metric_coeffs(scurve, pnormal);

	  scurve = pnormal;
	  fitness = fitness_pnormal; /* (do we need it?) */
	  update_c0(scurve, source, y_readout);

	}
      print_state(fitness_filepath_only_increases, step, fitness, scurve, 
		  step_size_adaptive, entropy, metric);

      if(entropy_indicator){
	entropy = shannon_entropy(adaptive, scurve, y_readout);
      }
      else{
	print_state(fitness_filepath, step, fitness, scurve, 
		    step_size_adaptive, 0, 0);
      }
      print_fitness_diagnostics(output_dir, step, scurve, y_readout, 
				  adaptive, step_size); 
      count_since_last_improvement = 0;
    } 
    else {
      ++count_since_last_improvement;
      if (count_since_last_improvement > nochange_max) 
	{
	  count_since_last_improvement = 0;
	  /* this last line is  a regular reduction of the step size  */
	  step_size_adaptive = step_size_adaptive / adapt_step_scaling;
	}
    }
    print_state(fitness_filepath, step, fitness, scurve, 
		step_size_adaptive, entropy, 0);
  }
 
  printf("adaptive number computations %d", nr_computations);
  return 0;

}
