#! /bin/sh

PROG_LIST_ALGOS="./scurve-hill ./scurve-hill-2d ./scurve-adaptive_random ./scurve-adaptive_random-2d ./scurve-iterated ./scurve-variable"
PROG_LIST_ALGOS_2D="./scurve-hill-2d ./scurve-adaptive_random-2d"


SRC_LIST="0.35,1000 0.74"
STEP_SIZE_LIST="0.83 2.21"

N_STEPS=150000

for src in $SRC_LIST
do
    cmd_fit="./scurve-fit-landscapes -s $src &"
    eval $cmd_fit
done

for source in $SRC_LIST
do
    for step in $STEP_SIZE_LIST
    do
	for prog in $PROG_LIST_ALGOS
	do
	    cmd="$prog -e $step -s $source -n $N_STEPS &"
	    eval $cmd
	done
	for prog2d in $PROG_LIST_ALGOS_2D
	do
	    cmd2d="$prog2d -s $source &"
	    eval $cmd2d
	done
    done
done

mail -s "scurve sweep mozart done" mark@galassi.org


