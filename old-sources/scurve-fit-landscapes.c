#include "scurve-types.h"
#include "prototypes.h"

#define N_DIM 4
#define N_SLICES (N_DIM*(N_DIM-1)/2) 
/* FIXME: must find simpler formula for SLICE2IND() */
#define SLICE2IND(i, j) (i == 0 ? (j-1) : (i == 1 ? (j+1) : (j+2)))

Source_type source;
double step_size, noise_percentage;
Poly scurve_true, scurve_initial;
int n_steps, real_shift;
int nr_computations;
/* procedure to loop through choice of coefficients, 
 * compute fitness, 
 * and what the algorithm would have chosen */

int main(int argc, char *argv[])
{
  char output_dir[MAX_FILENAME_LEN];
  double *y_readout = NULL; 
  setting_params(argc, argv, hill);
  /* FIXME doesn't matter which algo it is, hill set temporarily */
  make_directories(output_dir); 
  prepare_mask_and_readout(&y_readout, generate); 
  add_noise(noise_percentage, y_readout);

  FILE *fp_list[N_SLICES];
  char fname_list[N_SLICES][MAX_FILENAME_LEN];
  Poly scurve_list[N_SLICES];
  double fit[N_SLICES];

  int i, j;                     /* the coefficients that define the slice */
  int slice_ind;                /* the order index of the slice */

  for (i = 0; i < N_DIM; ++i) {
    for (j = i+1; j < N_DIM; ++j) {
      slice_ind = SLICE2IND(i, j);
      char src_spec[MAX_FILENAME_LEN];
      if (is_finite(source)) {
        sprintf(src_spec, "srcY%1.5g_Z%1.5g", source.y, source.z);
      } else {
        sprintf(src_spec, "srcA%1.5g", source.a);
      }
      sprintf(fname_list[slice_ind],
              "%s/fit_landscape_plane_%d_%d_%s", output_dir, i, j, src_spec);
      fp_list[slice_ind] = fopen(fname_list[slice_ind], "w");
      fprintf(fp_list[slice_ind], "## coefficient_0    coefficient_1"     
	      "coefficient_2   coefficient_3 fitness\n");
      print_metadata(fname_list[slice_ind]);
      
    }
  }
 
  /* the fulcrum around which we take the slices */
  Poly fulcrum_poly = scurve_true_make();
  double a, b;               /* the two coefficients we vary */
 
  for (i = 0; i < N_DIM; ++i) {
    for (j = i+1; j < N_DIM; ++j) {
      slice_ind = SLICE2IND(i, j);
      scurve_list[slice_ind] = fulcrum_poly; /* start at the fulcrum */
      for (a = coeff_min_bound; a < coeff_max_bound; a += 0.08) {
        for (b = coeff_min_bound; b < coeff_max_bound; b += 0.08) {
          scurve_list[slice_ind].c[i] = a/(i+1);
          scurve_list[slice_ind].c[j] = b/(j+1);	  
          fit[slice_ind] = calc_fitness(scurve_list[slice_ind], y_readout);
          int ci;               /* to loop through the coefficients */
          for (ci = 0; ci < N_DIM; ++ci) {
            fprintf(fp_list[slice_ind], "%g    ", scurve_list[slice_ind].c[ci]);
          }
          fprintf(fp_list[slice_ind], "%12g", fit[slice_ind]);
          fprintf(fp_list[slice_ind], "\n");
          fflush(fp_list[slice_ind]);
        }
      }
      fclose(fp_list[slice_ind]);
    }
  }

  return 0;
}


