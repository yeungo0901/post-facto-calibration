#include "scurve-types.h"
#include "prototypes.h"


/**
 * @file   scurve-poly.c
 * @author Mark Galassi <markgalassi@lanl.gov>
 * @date   Wed Mar 21 13:34:58 2012
 * @brief  routines to create and use the scurve polynomials
 */

/** 
 * creates the "true" s-curve, known only to god or the simulator
 * @return 
 */
/*
double y_readout_siman[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3715, 3678, 3617, 7490, 3799, 0, 0, 0, 0, 0, 3661, 3786, 3693, 7498, 3679, 3720, 3822, 3749, 3692, 7381, 3654, 3862, 3767, 3819, 3717, 0, 0, 0, 0, 3747, 3745, 3767, 3799, 3769, 3790, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 3765, 3768, 3820, 3828, 3678, 3864, 0, 0, 0, 0, 0, 0, 3782, 3802, 3656, 3671, 3764, 0, 0, 0, 0, 0, 0, 3785, 3811, 3837, 3699, 3798, 3788, 3771, 0, 3696, 3743, 3827, 3801, 3699, 0,0,0,0,0,0,0,0,0,0,0,0, 3754, 3753, 0, 3843, 3719, 3754, 3722, 0, 0, 0, 0, 0, 0, 3805, 3844, 3708, 3671, 0, 3634, 3745, 0, 0, 0, 0, 0, 0, 0, 3854, 3733, 0, 3720, 3803, 3744, 3773, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 3772, 0, 3750, 3773, 3759, 3767, 0, 0, 0, 0, 0, 0, 0, 0, 3662, 3724, 0, 3713, 3690, 3783, 3666, 0,0,0,0,0,0,0, 0,0,0,0,0,0,0, 3801, 3776, 0, 3822, 3738, 3777, 3829, 0, 0, 0, 0, 0, 0, 0, 0, 3723, 3834, 0, 3775, 3729, 3735, 3715, 3736, 0};
*/
Poly scurve_true_make()
{
  /* here's the invertible s-curve (see writeup), with c3 set to the
     overall distortion parameter.  Note that you can get the identity
     (no distortion) if you set distortion_c3 = 0. */
  double c3 = distortion_c3;
  Poly p = make_poly(0, (1.0+2*c3), -3.0*c3, c3);
  return p;
}


Poly update_c0(Poly scurve, Source_type source, double *y_readout){

  /* compute location of highest peak given by this scurve */
 double *y_real_guess = malloc(n_det_bins*sizeof(*y_real_guess));
  scurve_poly(y_readout, y_real_guess, scurve);
  double *convolution = malloc(n_det_bins*sizeof(*convolution));
  fill_convolution(y_real_guess, convolution);
  conv_sig_max convo = metric_peak_ratio(convolution);
  
  int peak = convo.max;
  
  double c0;

  if(is_finite(source)){
    double source_y_new = floor(1.0*peak*detector_height/n_det_bins);
    c0 = 0.5*(source_y_new - source.y);
    /* real_shift = floor(((source.y/detector_height)*n_det_bins)); */
  }
  else{
    double source_a_new = atan((1.0*peak*detector_height*focal_length/n_det_bins));
    c0 = 0.5*(source_a_new - source.a);
    /*real_shift = floor(((tan(source.a)*focal_length)*n_det_bins/detector_height));*/
  }

  scurve.c[0] = c0;

  free(y_real_guess);
  free(convolution);

  return scurve;
} 



/** 
 * applies a polynomial-parametrized scurve to a y_readout array 
 * @param y_readout input array
 * @param y_real resulting array with real data positions
 * @param poly the function to be applied
 */
void scurve_poly(double *y_readout, double *y_real, Poly poly)
{
  int j, new_j;
  double location, new_location;
  memset(y_real, 0, n_det_bins*sizeof(*y_real));
  /* looping through each pixel on the detector */
  for(j=0; j<n_det_bins; j++){ 
    /* calculate "physical location" */
    if(y_readout[j]){
      location = (1.0*j/(1.0*n_det_bins))*detector_height;
      /* apply scurve to the physical location */
      new_location = poly_map_point(poly, location);
      /* calculate the "new pixel" from the "new location" */
      new_j = (int) round(new_location*n_det_bins/detector_height);
      /* now we see if new_j is in range; if not we discard it.  this
         is a specific choice: we could also choose to tack it on to
         y_real[0] or y_real[n_det_bins-1] */
      if((new_j >= 0) && (new_j < n_det_bins)) {
	y_real[new_j] += y_readout[j];
      }
    }
  }
}

void scurve_poly_siman(double *y_real, Poly poly)
{
  int j, new_j;
  double location, new_location;
  memset(y_real, 0, n_det_bins*sizeof(*y_real));
  /* looping through each pixel on the detector */
  for(j=0; j<n_det_bins; j++){ 
    /* calculate "physical location" */
    if(y_readout_siman[j]){
      location = (1.0*j/(1.0*n_det_bins))*detector_height;
      /* apply scurve to the physical location */
      new_location = poly_map_point(poly, location);
      /* calculate the "new pixel" from the "new location" */
      new_j = (int) round(new_location*n_det_bins/detector_height);
      /* now we see if new_j is in range; if not we discard it.  this
         is a specific choice: we could also choose to tack it on to
         y_real[0] or y_real[n_det_bins-1] */
      if((new_j >= 0) && (new_j < n_det_bins)) {
	y_real[new_j] += y_readout_siman[j];
      }
    }
  }
}



/* procedure to take random step in hill climbing */
Poly hill_take_step(Poly prev_poly)
{
  int i;
  Poly p = prev_poly;
  for (i = 1; i < p.n; ++i) {
    double step = rand_in_bounds(step_size, -step_size);
    p.c[i] += step/(i+1);
  }
  return p;
}

Poly hill_take_step_2_plane(Poly prev_poly,  
			    int coeff_choice1, int coeff_choice2, 
			    double coeff_choice1_val, double coeff_choice2_val)
{
  int i,j;
  Poly p = prev_poly;
  /* first vary all coeffs */
  
  for (i = 1; i < p.n; ++i) {
    double step = rand_in_bounds(step_size, -step_size);
    p.c[i] += step/(i+1);
  }
  /* then find the other two coefficient which are not the ones in the
     function declaration */

  int j_need1=0, j_need2=0;
  for(j=1; j<p.n; ++j){
    if(j != coeff_choice1 && j != coeff_choice2){
      j_need1 = j;
    }
  }
  for(i=1; i<p.n; ++i){
    if(i != coeff_choice1 && i != coeff_choice2 && i!= j_need1){
      j_need2 = i;
    }
  }
  /* now assign to those two the coefficients corresponding to the
     real scurve */
  p.c[j_need1] = coeff_choice1_val;
  p.c[j_need2] = coeff_choice2_val;

  return p;  
}

/* this permutes the coefficients by a position */
/* FIXME: think of more clever ways */

Poly perturb_iterated(Poly prev_poly)
{
  int i;
  Poly p = prev_poly;

  for(i = 1; i< p.n; i++){
      p.c[i] = p.c[(i+1) % p.n];
  }
  return p;

}

Poly perturb_iterated_2_plane(Poly prev_poly, int coeff_choice1, 
			      int coeff_choice2, 
			      double coeff_choice1_val, 
			      double coeff_choice2_val)
{
  int i,j;
  Poly p = prev_poly;
  double aux;

  for (i = 1; i < p.n; ++i) {
    double step = rand_in_bounds(step_size, -step_size);
    p.c[i] += step/(i+1);
  }

  int j_need1=0, j_need2=0;
  for(j=1; j<p.n; ++j){
    if(j != coeff_choice1 && j != coeff_choice2){
      j_need1 = j;
    }
  }
  for(i=1; i<p.n; ++i){
    if(i != coeff_choice1 && i != coeff_choice2 && i!= j_need1){
      j_need2 = i;
    }
  }
  
  p.c[j_need1] = coeff_choice1_val;
  p.c[j_need2] = coeff_choice2_val;

  /* now perturb the other two */
  aux = p.c[coeff_choice2];
  p.c[coeff_choice2] = p.c[coeff_choice1];
  p.c[coeff_choice1] = aux;    
  
  return p;

}




Poly variable_take_step(Poly prev_poly, double *y_readout){

  Poly p = prev_poly;

  double neighborhood_bound_min = -0.5;
  double neighborhood_bound_max = 0.5;
  int nbhood_iter;
  int nbhoods = 8;
  int i_poly;
  int fn;
  double fitnesses_nbhoods[nbhoods];
    
  Poly p_list[nbhoods];
  for(fn=0;fn<nbhoods;fn++){
    p_list[fn] = p;
  }

  for(nbhood_iter = 0;nbhood_iter < nbhoods; nbhood_iter++){
    double nbhood_length = rand_in_bounds(neighborhood_bound_min, 
					  neighborhood_bound_max);
    for(i_poly=1;i_poly<p.n;i_poly++){
      p_list[nbhood_iter].c[i_poly]+= nbhood_length;
    }
    fitnesses_nbhoods[nbhood_iter] = calc_fitness(p_list[nbhood_iter], 
						  y_readout);
  }
  int j_poly, index=0;
  double max_fitness = fitnesses_nbhoods[0];
  for(j_poly=1;j_poly<nbhoods;j_poly++){
    if(fitnesses_nbhoods[j_poly] > max_fitness){
      max_fitness = fitnesses_nbhoods[j_poly];
      index = j_poly;
    }
  }

  return p_list[index];
      
}

Poly variable_take_step_2_plane(Poly prev_poly, double *y_readout, 
				int coeff_choice1, int coeff_choice2, 
				double coeff_choice1_val, 
				double coeff_choice2_val){
  /* FIXME: check this function */
  Poly p = prev_poly;

  double neighborhood_bound_min = -0.5;
  double neighborhood_bound_max = 0.5;
  int nbhood_iter;
  int nbhoods = 8;
  int i_poly, j_poly;
  int fn;
  double fitnesses_nbhoods[nbhoods];
    
  Poly p_list[nbhoods];
  for(fn=0;fn<nbhoods;fn++){
    p_list[fn] = p;
  }

  for(nbhood_iter = 0;nbhood_iter < nbhoods; nbhood_iter++){
    double nbhood_length = rand_in_bounds(neighborhood_bound_min, 
					  neighborhood_bound_max);

    for(i_poly=1;i_poly<p.n;i_poly++){      
      p_list[nbhood_iter].c[i_poly] = coeff_choice1_val;
    }

    for(j_poly = 1;j_poly<p.n;j_poly++)
      if(j_poly != coeff_choice1 || j_poly != coeff_choice2){
	p_list[nbhood_iter].c[j_poly]+=rand_in_bounds(nbhood_length, 
						      -nbhood_length);
      }

    fitnesses_nbhoods[nbhood_iter] = calc_fitness(p_list[nbhood_iter], 
						  y_readout);
  }
  int j_poly2, index=0;
  double max_fitness = fitnesses_nbhoods[0];
  for(j_poly2=1;j_poly2<nbhoods;j_poly2++){
    if(fitnesses_nbhoods[j_poly2] > max_fitness){
      max_fitness = fitnesses_nbhoods[j_poly2];
      index = j_poly2;
    }
  }

  return p_list[index];
      
}

/**
 * distorts a real y position into a distorted position; this could be
 * the kind of distortion that would be done by real detector hardware
 * and electronics, thus producing a "readout" value.
 * 
 * @param real_position 
 * 
 * @return readout position
 */
double distortion_func(double yreal)
{
  double readout_position;
  /* use the distortion that corresponds to the invertible s-curve in
     the writeup, with c3 set by distortion_c3 in constants.h */
  double c = distortion_c3;
  double radicand1 = (1.0/c)*(27*c*yreal*yreal - 54*c*yreal - 4*c*c*c+12*c*c+15*c+4);
  assert(radicand1 >= 0);
  double radicand2 = sqrt(radicand1) / (c*2*pow(3.0, 3.0/2.0)) + (yreal-1.0)/(2.0*c);
  /* now we must take care of something: if we use pow(x, 1.0/3.0) to
     take a cube root, it does not handle x < 0 so we play some
     tricks. Alternatively we can use the cbrt() routine (available in
     POSIX 2001 and C99) */
  /* double radicand2_abs = fabs(radicand2); */
  /* double radicand2_sign = (radicand2 > 0.0 ? 1.0 : -1.0); */
  /* double radicand2_cuberoot = radicand2_sign * pow(radicand2_abs, 1.0/3.0); */

  double radicand2_cuberoot = cbrt(radicand2);
  readout_position = 1.0 + radicand2_cuberoot + ((c-1.0)/(3.0*c)) / radicand2_cuberoot;

#ifndef NDEBUG
  /* a sanity check: make sure that the distortion and the scurve_true
     really cancel each other */
  Poly s_true = scurve_true_make();
  double y_real_mapped_back = poly_map_point(s_true, readout_position);
  double diff = yreal-y_real_mapped_back;
  printf("## DISTORTION: %g -> %g -> %g  --  %g\n",
         yreal, readout_position, y_real_mapped_back, diff);
  assert(fabs(diff) < 1.0e-8);
#endif /* NDEBUG */

  return readout_position;
}

/** 
 * the distortion is the inverse of the "true" scurve: it takes true
 * data (that is compatible with the mask and the angle) and maps it
 * to a readout -- this simulates the detector's distortion of
 * position measurement.
 *
 * this function applies it to the entire y_real to get a full
 * y_readout
 * 
 * @param y_real 
 * @param y_readout 
 */
void apply_distortion(double *y_real, double *y_readout)
{
  int j,new_j;
  double location, new_location;
  memset(y_readout, 0, n_det_bins*sizeof(*y_real));
  /* looping through each pixel on the detector */
  for(j=0; j<n_det_bins; j++){ 
    /* calculate "physical location" */
    if(y_real[j]){
      location = (1.0*j/(1.0*n_det_bins))*detector_height;
      /* apply scurve to the physical location */
      new_location = distortion_func(location);
      /* calculate the "new pixel */
      new_j = (int) round(new_location*n_det_bins/detector_height);
      /* now we see if new_j is in range; if not we discard it.  this
         is a specific choice: we could also choose to tack it on to
         y_real[0] or y_real[n_det_bins-1] */
      if((new_j >= 0) && (new_j < n_det_bins)) {
	y_readout[new_j] += y_real[j];
      }
    }
  }
}

/* helper functions to create polynomials */

/** 
 * a Poly is a 3-rd degree polynomial, so it has 4 coefficients; this
 * is a helper routine to create it.
 * 
 * @param c0 
 * @param c1 
 * @param c2 
 * @param c3 
 * 
 * @return 
 */
Poly make_poly(double c0, double c1, double c2, double c3)
{
  Poly p;
  p.n = 4;
  p.c[0] = c0;
  p.c[1] = c1;
  p.c[2] = c2;
  p.c[3] = c3;
  return p;
}
PolyN make_poly_1(double c0, double c1)
{
  PolyN p;
  p.n = 2;
  p.c = malloc(2*sizeof(*p.c));
  p.c[0] = c0;
  p.c[1] = c1;
  return p;
}
PolyN make_poly_2(double c0, double c1, double c2)
{
  PolyN p;
  p.n = 3;
  p.c = malloc(3*sizeof(*p.c));
  p.c[0] = c0;
  p.c[1] = c1;
  p.c[2] = c2;
  return p;
}
PolyN make_poly_3(double c0, double c1, double c2, double c3)
{
  PolyN p;
  p.n = 4;
  p.c = malloc(4*sizeof(*p.c));
  p.c[0] = c0;
  p.c[1] = c1;
  p.c[2] = c2;
  p.c[3] = c3;
  return p;
}
PolyN make_poly_4(double c0, double c1, double c2, double c3, double c4)
{
  PolyN p;
  p.n = 5;
  p.c = malloc(5*sizeof(*p.c));
  p.c[0] = c0;
  p.c[1] = c1;
  p.c[2] = c2;
  p.c[3] = c3;
  p.c[4] = c4;
  return p;
}

PolyN poly_copy(PolyN p_source)
{
  PolyN p_dest;
  p_dest.n = p_source.n;
  p_dest.c = malloc(p_dest.n*sizeof(*p_dest.c));
  memcpy(p_dest.c, p_source.c, p_dest.n*sizeof(*p_dest.c));
  return p_dest;
}

/** 
 * calculates the polynomial at the given x
 * 
 * @param p 
 * @param x 
 * 
 * @return 
 */
double poly_map_point(Poly p, double x)
{
  double y = 0;
  int i;
  for (i = 0; i < p.n; ++i) {
    y += p.c[i]*pow(x, (double) i);
  }
  return y;
}

/** 
 * tells us if 2 polynomials are different
 * 
 * @param p1 
 * @param p2 
 * 
 * @return 1 if p1 and p2 are different; 0 otherwise
 */
int poly_different(Poly p1, Poly p2)
{
  if (p1.n != p2.n) {
    return 1;
  }
  int i;
  for (i = 0; i < p1.n; ++i) {
    if (p1.c[i] != p2.c[i]) {
      return 1;
    }
  }
  /* if there are no differences then we return false */
  return 0;
}

/** 
 * prints a polynomials coefficients; this is meant to be inserted
 * into other printouts, so we do not print a newline
 * 
 * @param p 
 */
void poly_print(Poly p)
{
  int i;
  for (i = 0; i < p.n; ++i) {
    printf(" %7g ", p.c[i]);
  }
}

/** 
 * calls poly_print(p) and then puts out a newline
 * 
 * @param p 
 */
void poly_print_newline(Poly p)
{
  poly_print(p);
  printf("\n");
}
