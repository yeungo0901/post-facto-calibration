#ifndef _SCURVE_TYPES_H
#define _SCURVE_TYPES_H

typedef struct {
  int n;
  double *c;
} PolyN;

typedef struct {
  int n;
  double c[4];
} Poly;

typedef struct  conv_sig_max {double sig; int max;} conv_sig_max;

typedef struct {
  double a;
  double y;
  double z;
} Source_type;

/** specify whether data comes from a saved file or whether we create
    it on the go */
typedef enum {from_file, generate} DataOrigin;
typedef enum {hill, adaptive, iterated, variable} AlgorithmType;

#endif /* _SCURVE_TYPES_H */
