#ifndef _PROTOTYPES_H
#define _PROTOTYPES_H


#include <gsl/gsl_randist.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h> 
#include <gsl/gsl_siman.h>
#include <getopt.h>
#include <time.h>

#include "constants.h"
#include "scurve-types.h"

extern int mask[], n_steps, real_shift;
extern Source_type source;
extern double step_size, noise_percentage;
extern Poly scurve_initial, scurve_true;
extern int nr_computations;
extern double y_readout_siman[];
/* mask, y_real, legal indices prototypes, found in scurve-utils.c */
/*******************************************************************/
void scurve_check_setup();
double *generate_y_real();
int det_ind_finite_distance(double photon_angle);
int det_ind_legal_finite_distance(double photon_angle);
int det_ind_is_legal(int det_ind);
double *y_real2y_readout(double *y_real, Poly poly);
void read_y_readout(char *y_readout_fname, double **y_readout_p);
void prepare_mask_and_readout(double **readout_p, DataOrigin y_readout_origin);
int open_mask(double y);
void print_metadata(char *filepath);
/*********************************************************************/
/* some fitness prototypes, found in fitness.c */
/***************************************************/
double calc_fitness(Poly scurve, double *y_readout);
double rand_in_bounds(double min, double max);
void fill_convolution(double *input, double *result);
/*********************************************************/
/* polynomial prototypes, found in scurve-poly.c */
/********************************************************/
Poly make_poly(double c0, double c1, double c2, double c3);
PolyN make_poly_1(double c0, double c1);
PolyN make_poly_2(double c0, double c1, double c2);
PolyN make_poly_3(double c0, double c1, double c2, double c3);
PolyN make_poly_3(double c0, double c1, double c2, double c3);
PolyN make_poly_4(double c0, double c1, double c2, double c3, double c4);
PolyN poly_copy(PolyN p_source);
double poly_map_point(Poly p, double x);
int poly_different(Poly p1, Poly p2);
void poly_print(Poly p);
void poly_print_newline(Poly p);
void scurve_poly(double *y_readout, double *y_real, Poly poly);
void scurve_poly_siman(double *y_real, Poly poly);

Poly scurve_true_make();
void scurve_write(Poly scurve, char *filepath, char *comment);
Poly perturb_iterated(Poly prev_poly);
double distortion_func(double yreal);
void apply_distortion(double *y_real, double *y_readout);
conv_sig_max metric_peak_ratio(double *convolution);
Poly variable_take_step(Poly prev_poly, double *y_readout);
Poly hill_take_step_2_plane(Poly prev_poly, int coeff_choice1, 
			    int coeff_choice2, 
			    double coeff_choice1_val, 
			    double coeff_choice2_val);
Poly variable_take_step_2_plane(Poly prev_poly, double *y_readout, 
				int coeff_choice1, int coeff_choice2, 
				double coeff_choice1_val, 
				double coeff_choice2_val);

Poly perturb_iterated_2_plane(Poly prev_poly, int coeff_choice1, 
			      int coeff_choice2, 
			      double coeff_choice1_val, 
			      double coeff_choice2_val);
void assign_hyperplane(double *coeff_1, double *coeff_2, int i2, int j2);

/*********************************************************************/

/* printing prototypes, found in scurve-utils.c */
/********************************************************************/
void print_fitness_diagnostics(char *outdir, int step, Poly scurve,
                               double *y_readout, AlgorithmType t,
			       double step_size);
void print_state_header(char *fname);
void print_state(char *fname, int step, double fitness, Poly poly, 
		 double step_size, double entropy, double metric);
/********************************************************************/
/* adaptive prototypes, found in adaptive-utils.c */
/**********************************************************************/

void adapt_prep_step(Poly prev_poly, Poly *pnormal, Poly *plarge,
                     int step, double step_size_initial,
                     double *step_size_large);
void adapt_prep_step_2_plane(Poly prev_poly, Poly *pnormal, Poly *plarge,
			     int step, double step_size_initial,
			     double *step_size_large, int coeff_choice1, 
			     int coeff_choice2, double coeff_choice1_val, 
			     double coeff_choice2_val);
Poly hill_take_step(Poly prev_poly);
Poly adapt_take_step_choice(Poly poly);
Poly adapt_take_step(Poly scurve, int step, double *y_readout);
/**********************************************************************/
/* misc */
void parse_options(int argc, char *argv[]);
void setting_params(int argc, char *argv[], AlgorithmType t);
void make_directories(char *output_dir);
int is_finite(Source_type source);
int countChars( char* s, char c );
/***********************************************************************/
void add_noise(double noise_percentage, double *array);
int uniform();
double shannon_entropy(AlgorithmType t, Poly scurve, double *y_readout);

Poly update_c0(Poly scurve_test, Source_type source, double *y_readout);
double metric_coeffs(Poly scurve1, Poly scurve2);

#endif /* _PROTOTYPES_H */
