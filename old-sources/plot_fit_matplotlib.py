#! /usr/bin/env python

"""
make plots of a run -- shows some visualization of all the data in
the given run output directory.  this works by calling matplotlib to
create plots, then it saves them as PDF files.
"""

import sys
import glob
import os
from pprint import pprint
import string
import getopt
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
import numpy as np
import pylab
import matplotlib.font_manager as fm

markers = ('^', 'o', 'p', 's', '8', 'h', 'x', 'v')
colormap = cm.jet
correlation_style = 'full'              # 'full' or 'same'
global_fit_max = sys.float_info.min # initial values before we find them
global_fit_min = sys.float_info.max

def __MAIN__():
  ## first check command line options; these are "-i" to do an
  ## interactive show at the end, and then a set of directory names
  ## (if they are given); if no directory names are given then we use
  ## all directories that match the glob 'outdir_*eps*'
  interactive = False

  (opts, args) = getopt.getopt(sys.argv[1:], 'i', [])

  for o, a in opts:
    if o == '-i':                       # get a single date
      interactive = True

  timeline_fnames = []
  if not args:
    timeline_fnames = glob.glob('outdir_*/*fit_timeline')
    for f in timeline_fnames:
      end = f.index('/')
      outdir_name = f[0:end]
      print 'outdir_name', outdir_name
  else:
    for arg in args:
      #print args
      print len(arg)
      if len(arg) < 23:
      #if arg[:len('outdir_')] == 'outdir_':
        print arg[:len('outdir_')]
        print 'second case'
        timeline_fnames.extend(glob.glob(str(arg) + '/*fit_timeline'))
        outdir_name = str(arg)
        print 'outdir_name', outdir_name
      else:
        print 'third case'
        timeline_fnames.append(arg)
        end = arg.index('/')
        outdir_name = arg[0:end]
        print 'outdir_name', outdir_name

  font = {'family' : 'normal',
          'weight' : 'bold',
          'size'   : 10}

  timeline_fnames.sort()
  pprint('# processing this list of timeline-files:')
  pprint(timeline_fnames)
  ## now the actual work of generating the plots
  plot_fitnesses(outdir_name, timeline_fnames)
  ## now a little feature: if the command line says "interactive" 
  if interactive:
    plt.show()


def plot_fitnesses(outdir_name, timeline_fnames):

  """prepare pyplot figures for fitness and for step size"""
  fig1 = plt.figure(1)
  ax1 = fig1.add_subplot(1,1,1)
  ax1.set_xlabel('iteration')
  ax1.set_ylabel('fitness')
  plt.title('fitness as a function of iteration')
  fig2 = plt.figure(2)
  plt.xlabel('iteration')
  plt.ylabel('step size')
  plt.title('step size as a function of iteration')
  fig3 = plt.figure(3)
  ax3 = plt.axes()
  ax3.set_yticks([])            # no x or y ticks on the overall plots
  ax3.set_xticks([])
  plt.title('step size and fitness correlations')
  iters = {}
  fit = {}
  fit_delta = {}
  fit_maxes = {}
  fit_minima = {}
  step_size = {}
  total_fit = np.array([])
  total_plateau_duration = np.array([])
  #f = timeline_fname
  for f in timeline_fnames:    
    (iters[f], fit_list, step_size[f]) = load_timeline(f)
    if len(iters[f]) == 0:
      print 'file %s not found; continuing' % f
      continue
    fit[f] = np.array(fit_list)
    fit_delta[f] = fit_list2fit_delta(fit[f])
    (fit_maxes[f], fit_minima[f]) = (np.max(fit[f]), np.min(fit[f]))
    total_fit = np.append(total_fit, fit_delta[f])
    delta_iters = fit_list2fit_delta(iters[f])
    #total_step_size = np.append(total_step_size, step_size[f])
    total_plateau_duration = np.append(total_plateau_duration, delta_iters)
  ## now that we have all data, find the global max/min fitness
    global global_fit_max
    global global_fit_min
    global_fit_max = np.max(fit_maxes.values())
    global_fit_min = np.min(fit_minima.values())

  n_plots = len(iters.keys())
  #i = 0
  for timeline_ind,f in enumerate(timeline_fnames):
    if not fit.has_key(f):
      return

    fit_offset = fit[f] - global_fit_min
    fig1 = plt.figure(1)
    # FIXME: labels
   
    plt.plot(iters[f], fit[f],  label = f[f.find("/")+1:f.find("fit_timeline")], color = plt.get_cmap('jet')(float(timeline_ind)/(len(timeline_fnames))))
    print 'plotted'
    #plt.scatter(iters[f], fit[f], c=fit_offset, s=50*fit_offset,
     #           alpha=0.9, cmap=colormap, marker=markers[i%len(markers)],
     #           vmin = 0, vmax = global_fit_max-global_fit_min)
    #fig2 = plt.figure(2)
    #plt.plot(iters[f], step_size[f], label=f[len('fit_timeline_'):])
    #plt.scatter(iters[f], step_size[f], c=fit_offset, s=50*fit_offset,
     #           alpha=0.9, cmap=colormap, marker=markers[i%len(markers)],
     #           vmin = 0, vmax = global_fit_max-global_fit_min)
    '''
    fig3 = plt.figure(3)
    ## work with plateau duration
    delta_iters = fit_list2fit_delta(iters[f])
    xcorr_plateau_fit = np.correlate(delta_iters, fit_delta[f], correlation_style)
    delta_iters_mean_subtracted = delta_iters - np.mean(delta_iters)
    ## autocorrelations are more useful visually when you subtract out the mean
    acorr_plateau = np.correlate(delta_iters_mean_subtracted,
                                 delta_iters_mean_subtracted,
                                 correlation_style)
    fit_delta_mean_subtracted = fit_delta[f] - np.mean(fit_delta[f])
    acorr_fit = np.correlate(fit_delta[f], fit_delta[f], correlation_style)
    print f, len(step_size[f])
    for (hist, plot_no, name) in ((xcorr_plateau_fit, 1, 'xcorr plateau fit'),
                                  (acorr_fit, 3, 'acorr fit'),
                                  (acorr_plateau, 2, 'acorr plateau')):
      ax3 = fig3.add_subplot(n_plots, 3, 3*i+plot_no)
      plt.xticks(fontsize=5)
      plt.yticks(fontsize=4)
      plt.bar(range(len(hist)), hist,
              label=f[len('fit_timeline_'):] + ' ' + name)
      leg = ax3.legend(loc='best', shadow=True, ncol=1, prop={'size':4})
      leg.get_frame().set_alpha(0.7)
      '''
    #i += 1

  plt.figure(1)                         # switch to fitness
  #cbar = plt.colorbar(ticks = [0, global_fit_max-global_fit_min])
  #cbar.ax.set_yticklabels(['Low', 'High'])
  #cbar.set_label(r'fitness')
  plt.xscale('log')
  prepare_legend(plt, loc='best')
  plt.savefig(outdir_name + 'fit_sweep.pdf', format='pdf')
  print 'saved fit_sweep.pdf'
  plt.close()
  '''
  plt.figure(2)                         # switch to step size
  #cbar = plt.colorbar(ticks = [0, global_fit_max-global_fit_min])
  #cbar.ax.set_yticklabels(['Low', 'High'])
  #cbar.set_label(r'fitness')
  plt.xscale('log')
  plt.yscale('log')
  prepare_legend(plt)
  plt.savefig('step_size_sweep.pdf', format='pdf')
  print 'saved step_size_sweep.pdf'
  plt.figure(3)                         # switch to step distributions
  plt.savefig('step_acorr.pdf', format='pdf')
  print 'saved step_acorr.pdf'
  ## now add a single plot for a cross correlation of total step size
  ## and fitness pictures
  fig4 = plt.figure(4)
  ## first get the list of sort indices from the step size list, so we
  ## can resort both lists in sync; make them have 0 mean so that the
  ## autocorrelations are more informative
  sort_indices_fit = total_fit.argsort() # (NOTE: not used at this time)
  sort_indices_plateau = total_plateau_duration.argsort()

  total_fit_plateau_corr = np.correlate(total_fit[sort_indices_fit],
                                        total_plateau_duration[sort_indices_fit],
                                        correlation_style)
  ## adjust total_fit by sorting and subtracting the mean
  total_fit_adjusted = total_fit[sort_indices_plateau] - np.mean(total_fit)
  total_fit_acorr = np.correlate(total_fit_adjusted,
                                 total_fit_adjusted,
                                 correlation_style)
  total_plateau_duration_adjusted = (total_plateau_duration[sort_indices_plateau]
                                     - np.mean(total_plateau_duration))
  total_plateau_duration_acorr \
      = np.correlate(total_plateau_duration_adjusted,
                     total_plateau_duration_adjusted,
                     correlation_style)
  ## print total_fit_corr
  ax4 = fig4.add_subplot(5, 2, 1)
  plt.yscale('log')
  plt.bar(range(len(total_fit)), total_fit[sort_indices_plateau],
          label='log of fitness sorted by plateau duration')
  prepare_legend(plt, loc='upper right')
  ax4 = fig4.add_subplot(5, 2, 2)
  plt.yscale('log')
  plt.hist(total_fit[sort_indices_plateau], 100,
           label='log hist of combined fittness values')
  prepare_legend(plt, loc='upper right')
  ax4 = fig4.add_subplot(5, 2, 3)
  plt.yscale('log')
  plt.bar(range(len(total_fit)), total_plateau_duration[sort_indices_plateau],
          label='log of plateau durations')
  prepare_legend(plt, loc='upper left')
  ax4 = fig4.add_subplot(5, 2, 4)
  plt.yscale('log')
  plt.hist(total_plateau_duration[sort_indices_plateau], 100,
           label='log hist of plateau durations')
  prepare_legend(plt, loc='upper right')
  ax4 = fig4.add_subplot(5, 1, 3)
  plt.bar(range(len(total_fit_plateau_corr)), total_fit_plateau_corr,
          label='corr of combined fitness and plateau duration')
  prepare_legend(plt, loc='upper left')
  ax4 = fig4.add_subplot(5, 1, 4)
  plt.bar(range(len(total_fit_acorr)), total_fit_acorr,
          label='autocorrelation of combined fitness array')
  prepare_legend(plt, loc='upper left')
  ax4 = fig4.add_subplot(5, 1, 5)
  plt.bar(range(len(total_plateau_duration_acorr)), total_plateau_duration_acorr,
          label='autocorrelation of combined array of plateau durations')
  prepare_legend(plt, loc='upper left')
  plt.savefig('total_correlation.pdf', format='pdf')
  print 'saved total_correlation.pdf'
  '''
def plot_trajectories(timeline_fnames):
  """runs the plotting commands for trajectories.  this makes 3D plots
  that show the steps taken.  they are projected down into 4 3D plots,
  since the search space is 4 dimensional."""
  global global_fit_max
  global global_fit_min
  for triplet in ((5,6,7), (4, 6, 7), (4, 5, 7), (4, 5, 6)):
    fig = plt.figure()
    ax = Axes3D(fig)
    i = 0
    for f in timeline_fnames:
      (x, y, z, fit_list) = load_trajectory(f, triplet)
      if len(x) == 0:
        print 'file %s not found; continuing' % f
        continue
      fit = np.array(fit_list)
      fit_offset = fit - global_fit_min
      #FIXME LABELS
      ax.plot(x, y, z, label=f[len('fit_timeline_'):])
      p = ax.scatter(x, y, z, c=1000*fit_offset, s=30*fit_offset,
                     alpha=0.9, cmap=colormap,
                     vmin = 0, vmax = global_fit_max-global_fit_min,
                     marker=markers[i%len(markers)])
      i += 1
    ax.set_xlabel('coefficient $c_{%d}$' % (triplet[0]-4))
    ax.set_ylabel('coefficient $c_{%d}$' % (triplet[1]-4))
    ax.set_zlabel('coefficient $c_{%d}$' % (triplet[2]-4))
    cbar = fig.colorbar(p, ticks = [global_fit_min, global_fit_max],
                        shrink=0.5)
    cbar.ax.set_yticklabels(['Low', 'High']) # FIXME: doesn't work
    cbar.set_label(r'fitness')
    prepare_legend(plt)
    fname = 'trajectory_sweep_c%d_c%d_c%d.pdf' % tuple((np.array(triplet)-4).tolist())
    plt.savefig(fname, format='pdf')
    print 'saved %s' % fname

def load_trajectory(fname, triplet):
  """loads the trajectories from the timeline files, returns a
  tuple (x, y, z, fitness) of lists of points (and the fitness column)"""
  lines = open(fname).readlines()
  x = []
  y = []
  z = []
  fit = []
  for line in lines:
    if line[0] == '#':
      continue
    words = string.split(line)
    (xi, yi, zi, fit_i) = [float(words[i]) for i in triplet + (1,)]
    x.append(xi)
    y.append(yi)
    z.append(zi)
    fit.append(fit_i)
    
  return (x, y, z, fit)

def load_timeline(file):
  """loads the fitness and step size columns from a timeline file;
  returns three lists of values: (iters, fitness, step_size)"""
  lines = open(file)
  iters = []
  fitness = []
  step_size = []
  for line in lines:
    if line[0] == '#':
      continue
    words = string.split(line)
    iters.append(int(words[0]))
    fitness.append(float(words[1]))
    step_size.append(float(words[2]))
  return (iters, fitness, step_size)

def prepare_legend(plt, loc='upper left'):
  """prepare the legend; for our plots we make it somewhat transparent
  and we put it in the upper left corner and we choose a small font
  """
  plt.legend(loc=loc, shadow=True, ncol=2, prop={'size':6})
  plt.rc('font',**{'family':'serif','serif':['Computer Modern Roman'],'size':12})
  leg = plt.gca().get_legend()
  #ltext = leg.get_texts()
  #leg.get_frame().set_alpha(1.0)

def fit_list2fit_delta(fit_list):
  prev_list = np.zeros_like(fit_list)
  prev_list[1:] = fit_list[:-1]
  return fit_list-prev_list

if __name__ == '__main__':
  __MAIN__()
