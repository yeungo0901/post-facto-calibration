#! /usr/bin/env python

"""
make plots of an entire directory
"""

import sys
import glob
import os
import re
from pprint import pprint
import string
import getopt

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
import numpy as np
import math
# from matplotlib.mlab import griddata
from scipy.interpolate import griddata

markers = ('^', 'o', 'p', 's', '8', 'h', 'x', 'v')
colormap = cm.jet
correlation_style = 'same'              # 'full' or 'same'


global_fit_max = sys.float_info.min # initial values before we find them
global_fit_min = sys.float_info.max

def __MAIN__():

    timeline_fnames_incr = []
    timeline_fnames = []

    outdir = sys.argv[1]
    
    timeline_fnames_incr = glob.glob(outdir + '/*timeline_incr')
    timeline_fnames = glob.glob(outdir + '/*timeline')
    
    scurve_true = scurve_load_coefficients(outdir, 'scurve_true')
    mask = mask_load(outdir, 'mask')
        # EVOLUTION PLOTS
    make_evolution_plots(timeline_fnames_incr, scurve_true, mask, outdir)
    print('plotted evolution plots')
    
    epsilon_list = [0, 0.02, 0.03, 0.05, 0.06, 0.1, 0.11, 0.12, 0.16, 0.18, 0.24, 0.25, 0.3, 0.34, 0.38, 0.41, 0.45, 0.48, 0.52, 0.6, 0.7, 0.8, 0.9, 1.0]
    for timeline_ind, tf in enumerate(timeline_fnames):
        print(tf)
        # INFO MEASURES PLOTS
        plot_measures(timeline_fnames, timeline_ind, tf, epsilon_list, outdir)

    print('plotted measures')
    
    # LANDSCAPE_TRAJECTORY PLOTS
    plot_fit_landscapes_trajectories(outdir)
    print('plotted fit-traj')

def plot_measures(timeline_fnames, timeline_ind, tf, epsilon_list, outdir):
    # create arrays for each timeline
    H_list = []
    M_list = []
    for epsilon in epsilon_list:
        iters = {}
        fit_list = {}
        fit = {}
        step_size = {} 
        entropy = {}
        (iters[tf], fit[tf], step_size[tf], entropy[tf]) = load_timeline_info(tf)
    #print iters
    #print entropy
        s = {}
        s_prime = {}
        s = ''
        s_prime = ''
        for j in range(1,(len(fit[tf]))):
            if (fit[tf][j] - fit[tf][j-1]) < -epsilon:
                s+='2' #denoted as \bar{1} in paper
                s_prime+='2'
            elif math.fabs(fit[tf][j] - fit[tf][j-1]) <= epsilon: 
                s+='0'
                s_prime+='0'
            elif (fit[tf][j] - fit[tf][j-1]) > epsilon:
                s+='1'
                s_prime+='1'
               
    #print s
        if len(s) == 0:
            p_01 = p_02 = p_12 = p_10 = p_20 = p_21 = 0
        else:        
            p_01 = float(str(s).count('01'))/float(len(s))
            p_02 = float(str(s).count('02'))/float(len(s))
            p_12 = float(str(s).count('12'))/float(len(s))
            p_10 = float(str(s).count('10'))/float(len(s))
            p_20 = float(str(s).count('20'))/float(len(s))
            p_21 = float(str(s).count('21'))/float(len(s)) 
    #print p_12
        H = 0
        for probs in p_01, p_02, p_12, p_10, p_20, p_21:
      #print probs
            if probs != 0:
                H+=-(probs*math.log(probs,6)) 
        
    #H = H*100    

        s_prime_list = list(s_prime)
      
        for symbol in range(1,len(s_prime_list)):
            if s_prime_list[symbol] == s_prime_list[symbol-1]:
                s_prime_list[symbol] = '7'
                s_prime_list[symbol-1] = '7'
          
    #print s_prime_list
        s_prime = ''.join(s_prime_list)  
        s_prime = re.sub('7','',s_prime)
        s_prime = re.sub('0','',s_prime)
    #print 'lengths', len(s), len(s_prime)
        len_sprime_float = float(len(s_prime))
        len_s_float = float(len(s))
        M=0 
        if len_s_float !=0:
            M = len_sprime_float/len_s_float

    #M = M*100
            M_list.append(M)
            H_list.append(H)
    #print epsilon, H, M

    fig1= plt.figure(1)  
    ax1 = fig1.add_subplot(2,1,1)
    #print H_list
  #plt.xscale('log')
  #plt.yscale('log')
    plt.xlabel('epsilon')
    plt.ylabel('info content')
    plt.title('information content as a function of scale factor')
    plt.plot(epsilon_list,H_list,  label = tf[tf.find("/")+1:tf.find("fit_timeline")] , color = plt.get_cmap('jet')(float(timeline_ind)/(len(timeline_fnames))))
    prepare_legend(plt, loc='best')


    ax1 = fig1.add_subplot(2,1,2)  
  #plt.xscale('log')
  #plt.yscale('log')
    plt.xlabel('epsilon')
    plt.ylabel('partial info content')
    plt.title('partial information content as a function of scale factor')
    plt.plot(epsilon_list,M_list,label = tf[tf.find("/")+1:tf.find("fit_timeline")] , color = plt.get_cmap('jet')(float(timeline_ind)/(len(timeline_fnames))))
    prepare_legend(plt, loc='best')

    plt.savefig(outdir + '/info_measures.pdf', format = 'pdf')   

    
    fig2= plt.figure(2)
    ax2 = fig2.add_subplot(2,1,1)
  #fig3 = plt.figure(3)
    plt.xscale('log')
    plt.xlabel('iteration')
    plt.ylabel('entropy')
    plt.title('entropy vs. iteration')
    plt.plot(iters[tf],10*entropy[tf], label = tf[tf.find("/")+1:tf.find("fit_timeline")] )
    prepare_legend(plt, loc='best')
    #plt.savefig(outdir + '/entropy.pdf', format = 'pdf')
  
  #fig4 = plt.figure(4)

    fig2= plt.figure(2)
    ax2 = fig2.add_subplot(2,1,2)  
    plt.xscale('log')
    plt.xlabel('iteration')
    plt.ylabel('fitness')
    plt.title('fitness vs. iteration')
    plt.plot(iters[tf],fit[tf], label = tf[tf.find("/")+1:tf.find("fit_timeline")] )
    prepare_legend(plt, loc='best')
    
    plt.savefig(outdir + '/fit-entr.pdf', format = 'pdf')
    #print 'saved plot'



def plot_fit_landscapes_trajectories(outdir):

  ## find all landscape files 
  fnames_landscape = glob.glob(outdir + '/fit_landscape_plane*')
  #print fnames_landscape

  for fl in fnames_landscape:
    print(fl)
    run_id_start_pos = string.find(fl, 'fit_landscape_')
    run_id_land = fl[run_id_start_pos + len('fit_landscape_') : ]
    slice_compile = re.compile('.*plane_(.*?)src.*')
    slice_search = slice_compile.search(str(run_id_land))

    if not slice_search:
      print('dude, pattern not found')
      print('plane_(.*?)src')
      print(run_id_land)
    slice_current = slice_search.group(1)   
    slice_first_compile = re.compile('(.*?)_')
    slice_first = slice_first_compile.search(slice_current)
    if slice_first:
      coeff_land_1 = slice_first.group(1)
    slice_second_compile = re.compile('_(.*?)_')
    slice_second = slice_second_compile.search(slice_current)
    coeff_land_2 = slice_second.group(1)

    xl = {}
    yl = {}
    zl = {}
    xi = {}
    yi = {}
    X = {}
    Y = {}
    Z = {}
    ax = {}
    fig = {}
    surf = {}

    ## extract data from file that i need
    data_landscape = {}
    data_landscape[fl] = np.genfromtxt(fl)

    xl[fl] = data_landscape[fl][:,int(coeff_land_1)]
    if len(xl[fl]) == 0:
      print('file %s not found; continuing' % fl)
      continue

    yl[fl] = data_landscape[fl][:,int(coeff_land_2)]
    zl[fl] = data_landscape[fl][:,4]
      
    xi[fl] = np.linspace(min(xl[fl]),max(xl[fl]))
    yi[fl] = np.linspace(min(yl[fl]),max(yl[fl]))     
    X[fl],Y[fl] = np.meshgrid(xi[fl], yi[fl])
    Z[fl] = griddata(xl[fl],yl[fl],zl[fl],xi[fl],yi[fl])
    fig[fl] = plt.figure()
    ax[fl] = fig[fl].add_subplot(111, projection='3d')
    plt.xlabel('coefficient %s' % int(coeff_land_1))
    plt.ylabel('coefficient %s' % int(coeff_land_2))
    #plt.zlabel('Fitness')
    plt.title('fitness as a function of coefficient hyperplane')

    surf[fl] = ax[fl].plot_surface(X[fl], Y[fl], Z[fl], label = 'red', cstride=5, 
                                   cmap=cm.jet, alpha = 0.3)
    fnames = glob.glob(outdir + '/*fit_land_plane_%s_%s*' % (str(coeff_land_1), str(coeff_land_2)))
    ## now look at landscape slice files for different algorithms in the same hyperplane

    for timeline_ind, timeline_file in enumerate(fnames):
      #ax[fl].azim = -164
      #ax[fl].elev = 59
      print(timeline_file)
      data_timeline = {}
      xt = {}
      yt = {}
      Xt = {}
      xti = {}
      yti = {}
      Yt = {}
      Xt = {}
      fitt = {}
      Fitt = {}
      #print 'coeffs', coeff_land_1, coeff_land_2
      data_timeline[timeline_file] = np.genfromtxt(timeline_file) 
      xt[timeline_file] = data_timeline[timeline_file][:, int(coeff_land_1)+5]
      #print xt[timeline_file]
      if len(xt) == 0:
        print('file %s not found; continuing' % timeline_file)
        continue  
      yt[timeline_file] = data_timeline[timeline_file][:, int(coeff_land_2)+5]
      #print yt[timeline_file]
      fitt[timeline_file] = data_timeline[timeline_file][:,1]   
      label2 = timeline_file[timeline_file.find("/")+1:timeline_file.find("fit_land_plane")]
      #print label2
      
      ax[fl].plot(xt[timeline_file], yt[timeline_file],
                  zs=fitt[timeline_file],  label = label2, alpha=1.0, color = plt.get_cmap('jet')(float(timeline_ind)/(len(fnames))))
      ax[fl].scatter(xt[timeline_file], yt[timeline_file],
                     zs=fitt[timeline_file], c='green', alpha=0.7)

      plt.annotate('traj', xy=(2, 1), xytext=(3, 1.5), arrowprops=dict(facecolor='black', shrink=0.05))

    fname = '/trajectory_land_sweep_%s.pdf' % (run_id_land)

    plt.savefig(outdir + fname, format='pdf')
    prepare_legend(plt)
    #print 'saved %s' % fname
    plt.close()



def plot_fitnesses(timeline_fnames, outdir):

  """prepare pyplot figures for fitness and for step size"""
  fig1 = plt.figure(1)
  ax1 = fig1.add_subplot(1,1,1)
  ax1.set_xlabel('iteration')
  ax1.set_ylabel('fitness')
  plt.title('fitness as a function of iteration')
  fig2 = plt.figure(2)
  plt.xlabel('iteration')
  plt.ylabel('step size')
  plt.title('step size as a function of iteration')
  fig3 = plt.figure(3)
  ax3 = plt.axes()
  ax3.set_yticks([])            # no x or y ticks on the overall plots
  ax3.set_xticks([])
  plt.title('step size and fitness correlations')
  iters = {}
  fit = {}
  fit_delta = {}
  fit_maxes = {}
  fit_minima = {}
  step_size = {}
  total_fit = np.array([])
  total_plateau_duration = np.array([])
  #f = timeline_fname
  for f in timeline_fnames:    
    (iters[f], fit_list, step_size[f]) = load_timeline(f)
    if len(iters[f]) == 0:
      print('file %s not found; continuing' % f)
      continue
    fit[f] = np.array(fit_list)
    fit_delta[f] = fit_list2fit_delta(fit[f])
    (fit_maxes[f], fit_minima[f]) = (np.max(fit[f]), np.min(fit[f]))
    total_fit = np.append(total_fit, fit_delta[f])
    delta_iters = fit_list2fit_delta(iters[f])
    #total_step_size = np.append(total_step_size, step_size[f])
    total_plateau_duration = np.append(total_plateau_duration, delta_iters)
  ## now that we have all data, find the global max/min fitness
    global global_fit_max
    global global_fit_min
    global_fit_max = np.max(list(fit_maxes.values()))
    global_fit_min = np.min(list(fit_minima.values()))

  n_plots = len(list(iters.keys()))
  #i = 0
  for timeline_ind,f in enumerate(timeline_fnames):
    if f not in fit:
      return

    fit_offset = fit[f] - global_fit_min
    fig1 = plt.figure(1)
    # FIXME: labels
   
    plt.plot(iters[f], fit[f],  label = f[f.find("/")+1:f.find("fit_timeline")], color = plt.get_cmap('jet')(float(timeline_ind)/(len(timeline_fnames))))
    #print 'plotted'

  plt.figure(1)                         # switch to fitness
 
  plt.xscale('log')
  prepare_legend(plt, loc='best')
  plt.savefig(outdir+ '/fit_sweep.pdf', format='pdf')
  print('saved fit_sweep.pdf')
  plt.close()
 
def fit_list2fit_delta(fit_list):
  prev_list = np.zeros_like(fit_list)
  prev_list[1:] = fit_list[:-1]
  return fit_list-prev_list

def make_evolution_plots(timeline_fnames, scurve_true, mask, outdir):

  for tf in timeline_fnames: 

    print(tf)
    # create arrays for each timeline 
    iters = {}
    fit_list = {}
    fit = {}
    step_size = {} 
    initial = {}
    final = {}
    fit_differences = {}
    max_fit_differences = {}
    highest_jump1 = {}
    before_jump1 = {}
    index_max_differences = {}
    evol_files_initial = {}
    evol_files_before_jump1 = {}
    evol_files_jump1 = {}
    evol_files_final = {}

    total_length = len(tf)
    #print total_length
    length_end = tf.find("_fit_timeline_incr")
    size = total_length - length_end

    #print size
    run_id_tf = tf[tf.find("/")+1:tf.find("_fit_timeline_incr")]
    #run_id_tf = tf[int(total_length-1):int(size)]
    #print run_id_tf
    # load trajectory file
    (iters[tf], fit[tf], step_size[tf]) = load_timeline(tf)
    if len(iters[tf]) == 0:
      print('file %s not found; continuing' % tf)
      continue
 
    initial[tf] = iters[tf][0]
    final[tf] = iters[tf][-1]

    max_fit_differences[tf] = 0
    index_max_differences[tf] = 0
    
    # find the maximum jump in fitness and its corresponding index 
    for j in range(1,(len(fit[tf]))):
      if (fit[tf][j] - fit[tf][j-1]) > max_fit_differences[tf]:
        max_fit_differences[tf] = (fit[tf][j] - fit[tf][j-1])
        index_max_differences[tf] = j
        
    highest_jump1[tf] = iters[tf][index_max_differences[tf]]
    before_jump1[tf] = iters[tf][index_max_differences[tf]-1]
    

    # decide which files I want to look at, based on what I found above
    evol_files_initial[tf] = outdir + run_id_tf + '_step_' + str(initial[tf]) + '_all'
    evol_files_before_jump1[tf] = outdir + run_id_tf + '_step_' + str(before_jump1[tf]) + '_all'
    evol_files_jump1[tf] = outdir + run_id_tf + '_step_' + str(highest_jump1[tf]) + '_all'
    evol_files_final[tf] = outdir + run_id_tf + '_step_' + str(final[tf]) + '_all' 

    #make the plots for those files 
    make_single_iter_plots(evol_files_initial[tf], scurve_true,  mask)
    plt.savefig(outdir + '/' + run_id_tf + '_initial.pdf', format='pdf')
    plt.close()

    make_single_iter_plots(evol_files_before_jump1[tf], scurve_true, mask)
    plt.savefig(outdir + '/' + run_id_tf + '_before_jump1.pdf', format='pdf')
    plt.close()

    make_single_iter_plots(evol_files_jump1[tf], scurve_true,  mask)
    plt.savefig(outdir + '/' + run_id_tf +  '_jump1.pdf', format='pdf')
    plt.close()  

    make_single_iter_plots(evol_files_final[tf], scurve_true, mask)
    plt.savefig(outdir + '/' + run_id_tf +  '_final.pdf', format='pdf')
    plt.close()  

    #print 'done'


def make_single_iter_plots(fname, scurve_true,  mask):
  lines = open(fname).readlines()
  det_bins = np.array([])
  scurve = np.array([])
  y_real_guess = np.array([])
  conv = np.array([])
  for line in lines:
    if line[0] == '#':
      continue
    words = string.split(line)
    #print words
    det_bins = np.append(det_bins, int(words[0]))
    scurve = np.append(scurve, float(words[1]))
    y_real_guess = np.append(y_real_guess, int(words[2]))
    conv = np.append(conv, float(words[3]))
  fig = plt.figure()
  #fig.set_xlabel('detector pixel')
  ax1 = fig.add_subplot(4, 1, 1)
  ax1.set_ylabel('mask')
  plt.bar(np.arange(len(mask)), mask, label='mask')
  prepare_legend(plt, loc='lower center')

  ax2 = fig.add_subplot(4, 1, 2)
  ax2.set_ylabel('y_real_guess')
  plt.bar(det_bins, y_real_guess, label='y_real_guess')
  prepare_legend(plt, loc='lower center')

  ax3 = fig.add_subplot(4, 1, 3)
  ax3.set_ylabel('cross-correlation')
  plt.bar(det_bins, conv, label='cross-correlation')
  prepare_legend(plt, loc='lower center')

  ax4 = fig.add_subplot(4, 1, 4)
  ax4.set_ylabel('scurve(y)')
  plt.plot(scurve, label='this iteration')
  L = 2.0                     # FIXME: must get this from the metadata
  true_scurve_values = scurve_apply(scurve_true, det_bins, L)
  plt.plot(true_scurve_values, label='true s-curve')
  diff = scurve - true_scurve_values
  plt.plot(diff, label='difference')
  prepare_legend(plt, loc='upper left')


def scurve_load_coefficients(outdir, fname):
  """simple helper routine which returns a list of coefficients
  loaded from the given scurve file"""
  c = []
  lines = open(outdir + '/' + fname, 'r').readlines()
  for line in lines:
    if line[0] == '#':
      continue
    c.append(float(line))
  return c

def scurve_apply(s, y_array, L):
  """apply the scurve s to the array y_array; s is a list with the
  coefficients of the polynomial.  L is the scale for y."""
  result = np.zeros_like(y_array)
  for i in range(len(y_array)):
    y = y_array[i]*L/float(len(y_array))
    result[i] = s[0] + s[1]*y + s[2]*y*y + s[3]*y*y*y
  return result

def mask_load(outdir, fname):
  mask = []
  lines = open(outdir + '/' + fname).readlines()
  for line in lines:
    if line[0] == '#':
      continue
    val = int(line)
    assert(val == 0 or val == 1)
    mask.append(val)
  return mask

def prepare_legend(plt, loc='upper left'):
  """prepare the legend; for our plots we make it somewhat transparent
  and we put it in the upper left corner and we choose a small font
  """
  plt.legend(loc=loc, shadow=True, ncol=1, prop={'size':6})
  plt.rc('font',**{'family':'serif','serif':['Computer Modern Roman'],'size':12})
  leg = plt.gca().get_legend()
  #ltext = leg.get_texts()
  #leg.get_frame().set_alpha(0.2)

def load_timeline(file):
  """loads the fitness and step size columns from a timeline file;
  returns three lists of values: (iters, fitness, step_size)"""
  lines = open(file)
  iters = []
  fitness = []
  step_size = []
  for line in lines:
    if line[0] == '#':
      continue
    words = string.split(line)
    iters.append(int(words[0]))
    fitness.append(float(words[1]))
    step_size.append(float(words[2]))
  return (iters, fitness, step_size)

def load_timeline_info(fname):
  """loads the fitness and step size columns from a timeline file;
  returns three lists of values: (iters, fitness, step_size)"""
  lines = open(fname)
  iters = []
  fitness = []
  step_size = []
  entropy = []
  for line in lines:
    if line[0] == '#':
      continue
    words = string.split(line)
    iters.append(int(words[0]))
    fitness.append(float(words[1]))
    step_size.append(float(words[2]))
    entropy.append(float(words[3]))
  return (iters, fitness, step_size, entropy)

if __name__ == '__main__':
  __MAIN__()
