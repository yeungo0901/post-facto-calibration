#!/bin/sh

input_file=$1
input_file2=$2

GNUPLOT="gnuplot"
if [ -f /usr/lanl/bin/gnuplot ] ; then
  GNUPLOT="/usr/lanl/bin/gnuplot"
fi
${GNUPLOT} <<EOF
set terminal pdf enhanced font "Helvetica,4" size 6,3
set output "fit_entr_poly_epssweep_hill.pdf"
set multiplot layout 3,1
set xlabel "iteration" font "Helvetica,4"
set logscale x
set title "xi versus iteration for various step sizes" font "Helvetica,4"
set ylabel "xi" font "Helvetica,4"
plot for [i=0:9:2] '${input_file}' index i using 1:2 title columnheader(1) with lines 
set title "Fitness versus iteration for various step sizes" font "Helvetica,4"
set ylabel "fitness" font "Helvetica,4"
plot for [i=0:9:2] '${input_file}' index i using 1:3 title columnheader(1) with lines 
set title "Entropy versus iteration for various step sizes" font "Helvetica,4"
set ylabel "entropy" font "Helvetica,4"
plot [] [0:] for [i=0:9:2] '${input_file}' index i using 1:4 title columnheader(1) with lines
EOF
${GNUPLOT} <<EOF
set terminal pdf enhanced font "Helvetica,4" size 6,3
set output "fit_entr_poly_epssweep_adaptive.pdf"
set multiplot layout 3,1
set xlabel "iteration" font "Helvetica,4"
set logscale x
set title "xi versus iteration for various step sizes" font "Helvetica,4"
set ylabel "xi" font "Helvetica,4"
plot for [i=0:9:2] '${input_file2}' index i using 1:2 title columnheader(1) with lines 
set title "Fitness versus iteration for various step sizes" font "Helvetica,4"
set ylabel "fitness" font "Helvetica,4"
plot for [i=0:9:2] '${input_file2}' index i using 1:3 title columnheader(1) with lines 
set title "Entropy versus iteration for various step sizes" font "Helvetica,4"
set ylabel "entropy" font "Helvetica,4"
plot [] [0:] for [i=0:9:2] '${input_file2}' index i using 1:4 title columnheader(1) with lines
EOF
${GNUPLOT} <<EOF
set terminal pdf enhanced font "Helvetica,4" size 6,3
set output "step_vs_fitness_hill.pdf"
set title "Step sizes versus fitness" font "Helvetica,4"
set xlabel "step size" font "Helvetica,4"
set ylabel "fitness" font "Helvetica,4"
plot '${input_file}' using 5:2 with lines title "fitness vs step" 
EOF
${GNUPLOT} <<EOF
set terminal pdf enhanced font "Helvetica,4" size 6,3
set output "step_vs_fitness_adaptive.pdf"
set title "Step sizes versus fitness" font "Helvetica,4"
set xlabel "step size" font "Helvetica,4"
set ylabel "fitness" font "Helvetica,4"
plot '${input_file2}' using 5:2 with lines title "fitness vs step" 
EOF
exit 0
