#!/bin/sh

GNUPLOT="gnuplot"
if [ -f /usr/lanl/bin/gnuplot ] ; then
  GNUPLOT="/usr/lanl/bin/gnuplot"
fi

#make > /dev/null || exit 1
N_STEPS=5000

#LIST_GOOD_RUNS="0.16,0.83 0.16,2.21 0.41,0.83 0.41,0.12 0.11,0.12 0.25,75,0.83 0.25,75,0.83 0.25,75,2.21 0.39,15,0.83 0.39,15,2.21"
LIST_ALGOS=".././scurve-hill .././scurve-adaptive_random .././scurve-iterated .././scurve-variable"
LIST_GOOD_RUNS_TRIAL="0.88,0.9"
for good_run in $LIST_GOOD_RUNS_TRIAL
do
    for algo in $LIST_ALGOS
    do
	cmd="$algo -o $good_run -n $N_STEPS"
	eval $cmd
    done
done

cmd2=".././plot_run_evolution.py"
eval $cmd2
cmd3="mv */*final.pdf */*initial.pdf */*before_jump1.pdf */*jump1.pdf ../../notable-plots"
eval $cmd3

