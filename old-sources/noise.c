#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "scurve-types.h"
#include "prototypes.h"

void add_noise(double noise_percentage, double *array){

  /* add uniform noise to a percentage of the y_real_guess */
  int number_pixel_noise = floor(noise_percentage*n_det_bins);
  /*assert*/
  int i;
  for(i=0;i<=number_pixel_noise;i++){
    /* select a pixel at random within 1 and n_det_bins */
    int pixel_to_be_noised = random() % n_det_bins;
    array[pixel_to_be_noised]+=uniform();
  }

}

int uniform()
{
  return (random() % 25);
}
