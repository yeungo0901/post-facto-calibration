/**
 * @file   constants.h
 * @author Mark Galassi <markgalassi@lanl.gov>
 * @date   Tue Apr 10 12:58:12 2012
 * 
 * @brief  Overall constants.
 * 
 * 
 */

#ifndef _CONSTANTS_H
#define _CONSTANTS_H
#include <gsl/gsl_const_cgs.h>

#define MAX_LINE_LEN 10000
#define MAX_FILENAME_LEN 300

/* a flag for calc_fitness() */
#define DIAGNOSTICS_OFF 0
#define DIAGNOSTICS_ON 1

/* some characteristics for algorithms */
#define iter_max 2
#define nochange_max 30
/* #define stepsize_iter_factor 10 */
#define e_size 0.0001
#define large_step_period 10
#define adapt_step_small_scaling 1.1
#define adapt_step_large_scaling 2
#define adapt_step_scaling 1.01
#define step_size_default 0.65

#define n_steps_default 5000
#define metric_default ratio
#define n_photons_default 1500000
#define noise_percentage_default 0.0
/* this indicator suggest we calculate and print entropy if 1 */
#define entropy_default 0.0

/* the bounds where we generate coefficients */
#define rand_bounds_min (-0.2)
#define rand_bounds_max 0.2

#define entropy_indicator 1

/** @brief distortion_c3 is used to parametrize distortion; 0 means no
    distortion */
#define distortion_c3 0.2 /** @brief length between detector and mask */
#define focal_length  0.6 /** @brief angle of source when source is at infinity */
#define a_default 0.31 /** @brief detector pixels */
#define n_det_bins 400 /** @brief mask pixels */
#define n_mask_bins 70 /** @brief height of detector bars */
#define detector_height 2.0 /** @brief default source height when source is at finite distance */
#define y_default 1.0 /** @brief default source distance when source is at finite distance */
#define z_default -1.0

/* fitness landscape defaults */

#define coeff_min_bound -2.0
#define coeff_max_bound 2.0

#endif /* _CONSTANTS_H */
