#! /bin/sh

## make plots of a run -- shows some visualization of all the data in
## the given run output directory

outdir=$1
echo $#
if [ $# -ne 1 ]; then
    echo "usage: $0 outdir"
    exit 1
fi

## extract the search algorithm and suffix from the output directory name
search_algo=`echo $outdir | sed 's/outdir_\(.*\)-.*/\1/'`
echo "# search algorithm is $search_algo"
suffix=`echo $outdir | sed 's/.*_\(.*\)/\1/' | sed 's/\///'`

echo "# suffix is $suffix"

if [ ! -d $outdir ]; then
    echo "output data directory $outdir does not exist or is not a directory"
    exit 2
fi

## now make overall fitness plots and also show trajectories of the
## search moving through the space

input_file=${outdir}/fitness_timeline-${suffix}
pdf_file=`echo ${outdir}/fitness-${suffix}.pdf`
pdf_file_list="$pdf_file_list $pdf_file"

echo 'INPUT:' $input_file

gnuplot -e "set grid" \
    -e "set terminal pdf" \
    -e "set output '${pdf_file}'" \
    -e "set xlabel 'search iteration'" \
    -e "set ylabel 'fitness'" \
    -e "set logscale x" \
    -e "plot '${input_file}' using 1:2 with linespoints title '${search_algo}-${suffix}'"

pdf_file=${outdir}/search_path-${suffix}.pdf
pdf_file_list="$pdf_file_list $pdf_file"

gnuplot -e "set grid" \
    -e "set terminal pdf" \
    -e "set output '${pdf_file}'" \
    -e "splot '${input_file}' using 6:7:8 with linespoints title '${suffix}'" \
    -e "splot '${input_file}' using 5:7:8 with linespoints title '${suffix}'" \
    -e "splot '${input_file}' using 5:6:7 with linespoints title '${suffix}'" \
    -e "splot '${input_file}' using 5:6:8 with linespoints title '${suffix}'"

## now do a plot of y_real, convolution and scurve for each step
for step_file in ${outdir}/*_all
do
    step=`echo $step_file | sed 's/.*\/\(.*\)_all/\1/'`
    pdf_file=${outdir}/${step}_step_plots.pdf
    echo -n "$step "
    gnuplot -e "set terminal pdfcairo color enhanced font 'Helvetica,6'" \
        -e "set terminal pdf" \
        -e "set output '${pdf_file}'" \
        -e "set tmargin 0; set bmargin 1; set lmargin 3; set rmargin 3" \
        -e "unset ytics; unset xtics; set boxwidth 1.2 absolute" \
        -e "set style fill solid 1.00 border -1; set style data histogram" \
        -e "set style histogram cluster gap 0; set multiplot layout 3,1" \
        -e "set key autotitle column nobox samplen 1 noenhanced; set title 'step ${step}'" \
        -e "plot '${step_file}' using 3 t 'y_real' lc rgb 'blue'" \
        -e "plot '${step_file}' using 4 t 'convolution y_real with mask' lc rgb 'blue'" \
        -e "set yrange [0:2]; set style data line; set xtics 10; set ytics 0.5" \
        -e "set grid" \
        -e "set xlabel 'y readout'; set label 'fitness = [NOT YET]'" \
        -e "plot [] [-0.5:2] '${step_file}' using 1:2 with lines title 'scurve', '${step_file}' using 1:(\$2-(2.0/180.0)*(\$1)) with lines title 'scurve-identity'"
    pdf_file_list="$pdf_file_list $pdf_file"
done

## finally concatenate them together
echo "about to join all PDF files; the list is:"
echo "$pdf_file_list"
pdfjoin --quiet $pdf_file_list --outfile ${outdir}/${suffix}_combined.pdf
