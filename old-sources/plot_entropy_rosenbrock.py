#! /usr/bin/env python

""" make plots of two example fitness functions, one quadratic with a
single maximum y = -(x-a)*x and one quartic with two maxima y =
-(x-a)*(x-b)*x*(x-d).  These functions are plotted, as is the
hypothetical entropy of a hill-climbing search of radius epsilon at
each point.  """

from math import log, sqrt, pi, fabs, sin, cos
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
plt.rcParams.update({'legend.labelspacing':0.05})

def F(roots, x):
  fit = -1
  for key in roots.keys():
    fit *= (x - roots[key])
  return fit

def FRB(x, y):
  """Rosenbrock cost function; we take the negative of it (with an
  arbitrary offset) to get a fitness function"""
  F = (1.0-x)**2 + 100.0*(y-x*x)**2
  #F = 0
  #F = F + 0.3*np.sin(np.sqrt((x-1)*(x-1) + (y-1)*(y-1))*10.0)
  #F = F + 33.3*np.sin(50.0*np.sqrt(x*x + y*y))
  F = F + 700.0*np.sin(4.0*np.sqrt(x*x + y*y))
  F = - F             # ours is a fitness, not a cost, so we negate it
  return F

def __MAIN__():
  quad_roots = {'a': -1, 'b': 0}

  #hill2D_params = {'seed': 0, 'x_initial': -15.5, 'y_initial': -12.8,
  hill2D_params = {'seed': 0, 'x_initial': -1, 'y_initial': 3.1,
                   'sigma': 1.3, 'n_iter': 6150000}


  ## quad_hill_params = {'seed': 0, 'x_initial': 3,
  ##                     'sigma': 3, 'n_iter': 10000,
  ##                     'seed': 0}
  quad_hill_params = {'seed': 0, 'x_initial': 3,
                      'sigma': 0.3, 'n_iter': 4000}

  ## prepare parameters for the quartic search
  quart_roots = {'a': -3, 'b': -1.2, 'c': 0, 'd': 3}
  ## quart_hill_params = {'seed': 0, 'x_initial': -3.6,
  ##                      'sigma': 0.8, 'n_iter': 10000,
  ##                      'seed': 3} ## or 0.7
  quart_hill_params = {'seed': 0, 'x_initial': -3.6,
                       'sigma': 0.6, 'n_iter': 10000}
  ## now try the searching a oct polygon landscape
  oct_roots = {'a': -3, 'b': -2, 'c': 0, 'd': 2, 'e': 3, 'f': 5, 'g': 6, 'h': 7}
  ## oct_hill_params = {'seed': 0, 'x_initial': 7.15,
  ##                    'sigma': 3, 'n_iter': 100000000,
  ##                    'seed': 2}
  oct_hill_params = {'seed': 0, 'x_initial': 7.15,
                     'sigma': 3.5, 'n_iter': 100000}

  ## now make the plot panels for each function
  make_search_plot_panel2D(FRB, hill2D_params, 'rosenbrock')
  #make_search_plot_panel(-3.6, 3.2, quad_roots, quad_hill_params, 'quadratic')
  #make_search_plot_panel(-3.6, 3.2, quart_roots, quart_hill_params, 'quartic')
  #make_search_plot_panel(-3.15, 7.2, oct_roots, oct_hill_params, 'oct')

def make_search_plot_panel(xmin, xmax, roots, hill_params, nickname):
  ## values, roots, iters, climb_x, climb_F, climb_S, nickname, name):
  ## prepare the data: first the fitness landscape curves, then the
  ## hill-climbing progression data
  name = roots2name(roots)
  print '## %s -- %s' % (nickname, name)
  domain = np.arange(xmin, xmax, 0.05)
  vals = [F(roots, x) for x in domain]
  S = [hill_entropy(lambda(x): F(roots,x),x,0.05) for x in domain]
  (iters, climb_x, climb_F, climb_S) = hill_climb(F, roots, hill_params, nickname)

  ## now make the plots
  fig1 = plt.figure(1)
  plt.suptitle('fitness/entropy, landscape/evolution for %s' % name)
  ## plot landscape (i.e. F(x) versus x)
  plt.subplot(2, 2, 1)
  plt.ylabel('fitness')
  plt.grid()
  plt.plot(domain, vals, label='F(x)')
  plt.scatter([roots[k] for k in roots.keys()], [0 for k in roots.keys()],
                   c='red', s=100, label='zero crossings')
  size_array = ((climb_F - np.min(climb_F))
                / (np.max(climb_F) - np.min(climb_F)))
  plt.scatter(climb_x, climb_F, c='green', alpha=0.3,
                   s=30*size_array, label='search points')
  plt.plot(climb_x, climb_F, c='green', alpha=0.5)
  ## and now way too much work to make the legend look OK
  leg = plt.legend(loc='lower center')
  ltext = leg.get_texts()
  leg.markerscale=0.001
  leg.get_frame().set_alpha(0.4)
  plt.setp(ltext, fontsize='small')

  ## now fitness as a function of iteration
  plt.subplot(2, 2, 2)
  plt.grid()
  plt.semilogx(iters, climb_F, label='fitness')

  ## now S(x)
  plt.subplot(2, 2, 3)
  plt.grid()
  plt.plot(domain, S, label=r'$S(x)$')
  plt.xlabel('x')
  plt.ylabel('entropy')

  plt.subplot(2, 2, 4)
  plt.grid()
  plt.semilogx(iters, climb_S, label='S(iteration)')
  plt.xlabel('iteration')

  ## done with plots; save them
  plt.savefig('entropy_toy_%s.pdf' % nickname, format='pdf')
  plt.close()

def make_search_plot_panel2D(F, hill_params, nickname):
  print '## %s' % nickname
  (xmin, xmax, ymin, ymax) = (-2, 2.5, -2, 3.5)
  ## now use numpy's "meshgrid" paradigm for making a grid and
  ## evaluating a function on it
  xdomain = np.arange(xmin, xmax, 0.05) # should be 0.15 for nice plots; 0.05 is very nice
  ydomain = np.arange(ymin, ymax, 0.05)
  xgrid, ygrid = np.meshgrid(xdomain, ydomain)
  #vals = [F(roots, x) for x in domain]
  fit_values = F(xgrid, ygrid)
  sigma = hill_params['sigma']
  hill_entropy_grid_func = np.vectorize(lambda a, b: hill_entropy2D(F, a, b, sigma))
  S = hill_entropy_grid_func(xgrid, ygrid)
  #S = hill_entropy2D(xgrid, ygrid, lambda a, b: F(a, b),x,y,hill_params['sigma'])
  ## (iters, climb_x, climb_F, climb_S) = hill_climb(F, roots, hill_params, nickname)
  (iters, climb_x, climb_y, climb_F, climb_S) = hill_climb2D(F, hill_params, nickname)

  ## now make the plots
  fig1 = plt.figure(1)
  plt.suptitle('fitness/entropy, landscape/evolution for %s' % nickname)
  ## plot landscape (i.e. F(x) versus x)
  ax = fig1.add_subplot(2,2,1, projection='3d')
  ax.azim = -164
  ax.elev = 59
  surf = ax.plot_surface(xgrid, ygrid, fit_values, label='rosenbrock',
                         cstride=5, cmap=cm.jet, alpha=0.3)
  #fig1.colorbar(surf, shrink=0.5, aspect=5)
  ## plt.zlabel('fitness')
  ## plt.grid()
  size_array = ((climb_F - np.min(climb_F))
                / (np.max(climb_F) - np.min(climb_F)))
  #n_scatter_pts = 200
  n_scatter_pts = 0
  print iters[-n_scatter_pts:]
  print climb_x[-n_scatter_pts:]
  print climb_y[-n_scatter_pts:]
  ax.scatter(climb_x[-n_scatter_pts:], climb_y[-n_scatter_pts:],
             zs=climb_F[-n_scatter_pts:], c='green', alpha=1.0,
             s=90*size_array[-n_scatter_pts:], label='search points')
  ax.plot(climb_x[-n_scatter_pts:], climb_y[-n_scatter_pts:],
          zs=climb_F[-n_scatter_pts:], c='red', alpha=1.0, label='search path')

  ## now fitness as a function of iteration
  plt.subplot(2, 2, 2)
  plt.grid()
  plt.semilogx(iters, climb_F, label='fitness')

  ## ## now S(x)
  ax = fig1.add_subplot(2, 2, 3, projection='3d')
  ax.azim = -164
  ax.elev = 59
  #plt.grid()
  surf = ax.plot_surface(xgrid, ygrid, S, label=r'$S(x,y)$', alpha=1.0)
  ax.scatter(climb_x[-n_scatter_pts:], climb_y[-n_scatter_pts:],
             zs=climb_S[-n_scatter_pts:], c='green', alpha=1.0,
             s=90*size_array[-n_scatter_pts:], label='search points')
  ax.plot(climb_x[-n_scatter_pts:], climb_y[-n_scatter_pts:],
          zs=climb_S[-n_scatter_pts:], c='red', alpha=1.0, label='search path')
  #ax.plot(domain, S, label=r'$S(x)$')
  ## ax.xlabel('x')
  ## ax.ylabel('y')
  ## ax.zlabel('S')

  plt.subplot(2, 2, 4)
  plt.grid()
  plt.semilogx(iters, climb_S, label='S(iteration)')
  plt.xlabel('iteration')

  ## done with plots; save them
  plot_fname = 'entropy_%s.pdf' % nickname
  print 'saving to file %s' % plot_fname
  plt.savefig(plot_fname, format='pdf')
  plt.show()                       # for now also have interactive use
  plt.close()

def hill_climb(F_base, roots, params, nickname):
  """simple hill-climbing algorithm; returns a tuple of
  of lists of positions, fitnesses, entropies"""
  ## define the fitness function based on the roots of this polynomial
  F = lambda(x): F_base(roots, x)
  np.random.seed(params['seed'])
  x = params['x_initial']
  sigma = params['sigma']
  n_iter = params['n_iter']
  fit_prev = F(x)
  ## prepare arrays to save our data; KLUDGE: put first entry twice
  ## since it's logscale (yuck)
  iter_vals = np.array([0, 1])
  x_vals = np.array([x, x])
  F_vals = np.array([fit_prev, fit_prev])
  S = hill_entropy(F, x, sigma)
  S_vals = np.array([S, S])
  print 'hill_ixFS_%s:' % nickname, iter_vals[-1], x_vals[-1], F_vals[-1], S_vals[-1]
  for i in range(1, n_iter):
    #step = np.random.multivariate_normal([0, 0], [[sigma, 0], [0, 100]])
    #step = np.random.normal(0, sigma)
    step = np.random.gumbel(0, sigma)
    x_try = x + step
    fit = F(x_try)
    if fit >= fit_prev:
      ## we now prepare to append the new values, but we note that if
      ## more than one iteration has passed since the last one, we
      ## also add the (i-1) set of values
      if (i-1) > iter_vals[-1]:
        iter_vals = np.append(iter_vals, i-1)
        x_vals = np.append(x_vals, x_vals[-1])
        F_vals = np.append(F_vals, F_vals[-1]) # we must recalculate F and S here
        S_vals = np.append(S_vals, S_vals[-1])
        print 'Jhill_ixFS_%s:' % nickname, iter_vals[-1], x_vals[-1], F_vals[-1], S_vals[-1]
        #print '## jump!', i, iter_vals, S_vals
      ## now calculate the new values and append them
      S = hill_entropy(F, x_try, sigma)
      fit_prev = fit
      x = x_try
      iter_vals = np.append(iter_vals, i)
      x_vals = np.append(x_vals, x_try)
      F_vals = np.append(F_vals, fit)
      S_vals = np.append(S_vals, S)
      print 'hill_ixFS_%s:' % nickname, i, x_try, fit, S
  return (iter_vals, x_vals, F_vals, S_vals)

def hill_climb2D(F_base, params, nickname):
  """simple hill-climbing algorithm; returns a tuple of
  of lists of positions, fitnesses, entropies"""
  ## define the fitness function based on the roots of this polynomial
  F = lambda x, y: F_base(x, y)
  np.random.seed(params['seed'])
  x = params['x_initial']
  y = params['y_initial']
  sigma = params['sigma']
  n_iter = params['n_iter']
  fit_prev = F(x, y)
  ## prepare arrays to save our data; KLUDGE: put first entry twice
  ## since it's logscale (yuck)
  iter_vals = np.array([0, 1])
  x_vals = np.array([x, x])
  y_vals = np.array([y, y])
  F_vals = np.array([fit_prev, fit_prev])
  S = hill_entropy2D(F, x, y, sigma)
  S = -0.5*log(0.5)               # FIXME: I don't yet have 2D entropy
  S_vals = np.array([S, S])
  print 'hill_ixFS_%s:' % nickname, iter_vals[-1], x_vals[-1], y_vals[-1], F_vals[-1], S_vals[-1]
  for i in range(1, n_iter):
    #step = np.random.multivariate_normal([0, 0], [[sigma, 0], [0, 100]])
    #step = np.random.normal(0, sigma)
    stepx = np.random.gumbel(0, sigma)
    x_try = x + stepx
    stepy = np.random.gumbel(0, sigma)
    y_try = y + stepy
    fit = F(x_try, y_try)
    if fit >= fit_prev:
      ## we now prepare to append the new values, but we note that if
      ## more than one iteration has passed since the last one, we
      ## also add the (i-1) set of values
      if (i-1) > iter_vals[-1]:
        iter_vals = np.append(iter_vals, i-1)
        x_vals = np.append(x_vals, x_vals[-1])
        y_vals = np.append(y_vals, y_vals[-1])
        F_vals = np.append(F_vals, F_vals[-1]) # we must recalculate F and S here
        S_vals = np.append(S_vals, S_vals[-1])
        print 'Jhill_ixFS_%s:' % nickname, iter_vals[-1], x_vals[-1], y_vals[-1], F_vals[-1], S_vals[-1]
        #print '## jump!', i, iter_vals, S_vals
      ## now calculate the new values and append them
      S = hill_entropy2D(F, x_try, y_try, sigma)
      fit_prev = fit
      x = x_try
      y = x_try
      iter_vals = np.append(iter_vals, i)
      x_vals = np.append(x_vals, x_try)
      y_vals = np.append(y_vals, y_try)
      F_vals = np.append(F_vals, fit)
      S_vals = np.append(S_vals, S)
      print 'hill_ixFS_%s:' % nickname, i, x_try, y_try, fit, S
  return (iter_vals, x_vals, y_vals, F_vals, S_vals)

def hill_entropy(F, x, sigma):
  """calculate and return the entropy for the hill-climbing probability
  of taking a step; this is at a given point x and with a given fitness
  function F"""
  ## first save the random number state (and restore it before
  ## exiting), so that the random numbers generated here don't
  ## interfere with the reproducibility of the search
  random_state = np.random.get_state()
  ## prepare the mu and beta parameters for the gumbel distribution,
  ## based on the mean and sigma (0 and sigma)
  sixoverpi2 = (6.0/(pi*pi))
  beta = sqrt(6)*sigma/pi
  mu = 0 - 0.57721 * sixoverpi2 * sigma
  #n_tries = 1000*1000
  n_tries = 10*1000
  n_yes = 0
  n_no = 0
  F0 = F(x)
  steps = np.random.gumbel(mu, beta, n_tries)
  for step in steps:
    directed_step = step*(-1.0 + 2.0 * np.random.randint(0, 2))
    x_try = x + directed_step
    if F(x_try) >= F0:
      n_yes += 1
    else:
      n_no += 1
  p = float(n_yes)/float(n_tries)
  if p == 0:
    S = 0
  else:
    S = - p * log(p)
  np.random.set_state(random_state)
  #print 'xFpS:', x, F0, p, S
  return S

def hill_entropy2D(F, x, y, sigma):
  #return 0.3                            # FIXME: short-circuit for speed
  """calculate and return the entropy for the hill-climbing probability
  of taking a step; this is at a given point x and with a given fitness
  function F"""
  ## first save the random number state (and restore it before
  ## exiting), so that the random numbers generated here don't
  ## interfere with the reproducibility of the search
  random_state = np.random.get_state()
  ## prepare the mu and beta parameters for the gumbel distribution,
  ## based on the mean and sigma (0 and sigma)
  sixoverpi2 = (6.0/(pi*pi))
  beta = sqrt(6)*sigma/pi
  mu = 0 - 0.57721 * sixoverpi2 * sigma
  #n_tries = 1000*1000
  n_tries = 1*1000
  n_yes = 0
  n_no = 0
  F0 = F(x, y)
  steps = np.random.gumbel(mu, beta, n_tries)
  for step in steps:
    ## randomize the direction of the step
    random_angle = np.random.uniform(0, 2.0*pi)
    directed_stepx = sin(random_angle)
    directed_stepy = cos(random_angle)
    x_try = x + directed_stepx
    y_try = x + directed_stepy
    if F(x_try, y_try) >= F0:
      n_yes += 1
    else:
      n_no += 1
  p = float(n_yes)/float(n_tries)
  if p == 0:
    S = 0
  else:
    S = - p * log(p)
  np.random.set_state(random_state)
  #print 'xFpS:', x, F0, p, S
  return S

def roots2name(roots):
  """takes the list of roots and converts it into a polynomail function name"""
  name = 'F(x) = -'
  root_keys = roots.keys()
  root_keys.sort()
  for r in root_keys:
    if roots[r] < 0:
      name += '(x+%g)' % fabs(roots[r])
    else:
      name += '(x-%g)' % roots[r]
  return name

if __name__ == '__main__':
  __MAIN__()
