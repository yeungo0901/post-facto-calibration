#include "prototypes.h"
#include "constants.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <gsl/gsl_siman.h>

Source_type source;
double step_size, noise_percentage;
Poly scurve_true, scurve_initial;
int n_steps, real_shift;
int nr_computations;



/* The first example, in one dimensional Cartesian space, sets up an
   energy function which is a damped sine wave; this has many local
   minima, but only one global minimum, somewhere between 1.0 and
   1.5. The initial guess given is 15.5, which is several local minima
   away from the global minimum. */

/*
./scurve-siman | awk '!/^#/ {print $1, $4}' | graph -y 1.34 1.4 -W0 -X generation -Y position | plot -Tps > siman-test.eps

./scurve-siman | awk '!/^#/ {print $1, $5}' | graph -y -0.88 -0.83 -W0 -X generation -Y energy | plot -Tps > siman-energy.eps

*/

/* you can build and run this program and view its results with:
   gcc scurve-siman.c -o scurve-siman -lgsl -lgslcblas -o scurve-siman
   ./scurve-siman > scurve-siman.out
   gnuplot ## (following commands are in gnuplot)
   plot [4:] './siman-rosenbrock' using 1:4 with lines
   replot './siman-rosenbrock' using 1:5 with lines
   plot [4:] './siman-rosenbrock' using 1:6 with lines
   The first two plots show how x and y evolve with the search;
   the third shows energy/cost evolving with the search.
*/



/* set up parameters for this simulated annealing run */
     
/* how many points do we try before stepping */
#define N_TRIES 20
     
/* how many iterations for each T? */
#define ITERS_FIXED_T 100
     
/* max step size in random walk */
#define STEP_SIZE 0.05
     
/* Boltzmann constant */
#define K 1.0                   
     
/* initial temperature */
#define T_INITIAL 0.8
     
/* damping factor for temperature */
#define MU_T 1.010
#define T_MIN 2.0e-10
     
gsl_siman_params_t params 
= {N_TRIES, ITERS_FIXED_T, STEP_SIZE,
   K, T_INITIAL, MU_T, T_MIN};
     
gsl_siman_params_t params2
= {N_TRIES, ITERS_FIXED_T, STEP_SIZE,
   K, T_INITIAL, MU_T, T_MIN};
 

double E2(void *xp)
{

  /* here is where I am declaring y_readout and y_real */
  double x = ((double *) xp)[0];
  double y = ((double *) xp)[1];
  /* calculate the Rosenbrock function */
     
  double E = (1-x)*(1-x) + 100.0*(y-x*x)*(y-x*x);
  /* now add a bit of a ripple pattern so that we get local minima */
  /* double r2 = (x-1)*(x-1) + (y-1)*(y-1); */
  double r2 = x*x + y*y;
  double wiggle = 700.0*sin(4.0*sqrt(r2));
  E = E + wiggle;
  return E;
}


     
double M2(void *xp, void *yp)
{
  double *x = (double *) xp;
  double *y = (double *) yp;
  double r2 = (x[0] - y[0])*(x[0] - y[0]) + (x[1] - y[1])*(x[1] - y[1]);
  return sqrt(r2);
}


void S2(const gsl_rng * r, void *xp, double step_size)
{
  double *old_x = ((double *) xp);
  double new_x[2];
     
  double u0 = gsl_rng_uniform(r);
  double u1 = gsl_rng_uniform(r);
  new_x[0] = u0 * 2 * step_size - step_size + old_x[0];
  new_x[1] = u1 * 2 * step_size - step_size + old_x[1];

  memcpy(xp, &new_x, sizeof(new_x));
}


void P2(void *xp)
{
  double x = ((double *) xp)[0];
  double y = ((double *) xp)[1];
  /* double *xp_array = (double *) xp; */
  printf ("%12g    %12g", x, y);
}

double E4(void *xp)
{

  /* here is where I am declaring y_readout and y_real */
  /* double *y_readout = NULL; */

  double c0 = ((double *) xp)[0];
  double c1 = ((double *) xp)[1];
  double c2 = ((double *) xp)[2];
  double c3 = ((double *) xp)[3];

  /* prepare_mask_and_readout(&y_readout, generate);*/

  nr_computations++;

  /* Poly scurve = (Poly*)(xp); */
  Poly scurve = make_poly(c0, c1, c2, c3);
  
  double *y_real_guess = malloc(n_det_bins*sizeof(*y_real_guess));
  scurve_poly_siman(y_real_guess, scurve);
  double *convolution = malloc(n_det_bins*sizeof(*convolution));
  fill_convolution(y_real_guess, convolution);
  conv_sig_max convo = metric_peak_ratio(convolution);
  double fitness = convo.sig;

  free(y_real_guess);
  free(convolution);

  return -fitness;
  

}

double M4(void *xp, void *yp)
{
  double *scurve1 = (double *) xp;
  double *scurve2 = (double *) yp;

  double r4 = (scurve1[0] - scurve2[0])*(scurve1[0] - scurve2[0]) + (scurve1[1] - scurve2[1])*(scurve1[1] - scurve2[1])/70.0 + (scurve1[2] - scurve2[2])*(scurve1[2] - scurve2[2])/pow(70.0,2.0) + (scurve1[3] - scurve2[3])*(scurve1[3] - scurve2[3])/pow(70.0,3.0);

  return sqrt(r4);
}

void S4(const gsl_rng * r, void *xp, double step_size)
{
  double *old_x = ((double *) xp);
  double new_x[4];
     
  double u0 = gsl_rng_uniform(r);
  double u1 = gsl_rng_uniform(r);
  double u2 = gsl_rng_uniform(r);
  double u3 = gsl_rng_uniform(r);
  
  new_x[0] = u0 * 2 * step_size - step_size + old_x[0];
  new_x[1] = u1 * 2 * step_size - step_size + old_x[1];
  new_x[2] = u2 * 2 * step_size - step_size + old_x[2];
  new_x[3] = u3 * 2 * step_size - step_size + old_x[3];

  memcpy(xp, &new_x, sizeof(new_x));
}

void P4(void *xp)
{
  double c0 = ((double *) xp)[0];
  double c1 = ((double *) xp)[1];
  double c2 = ((double *) xp)[2];
  double c3 = ((double *) xp)[3];
    
  /* double *xp_array = (double *) xp; */
  printf ("%12g   %12g  %12g  %12g", c0, c1, c2, c3);
}


int main(int argc, char *argv[])
{

  const gsl_rng_type * T;
  gsl_rng * r;
  setting_params(argc, argv, hill);
  /*prepare_mask_and_readout(&y_readout, generate);*/

  /* double x_initial[4]; */
  /* Poly scurve_true = scurve_true_make(); */
  /* memcpy(x_initial, scurve_true.c, sizeof(x_initial)); */
  /* double x_initial[] = {0, 1.4, -0.6, 0.2}; /\* true scurve *\/ */
  double x_initial[] = {0, 2, 0, 0}; /* linear */

  scurve_check_setup();

  /* double x_initial[] = {-2.0, -1.0}; */

  gsl_rng_env_setup();
     
  T = gsl_rng_default;
  r = gsl_rng_alloc(T);
 
  /*FILE *fp = freopen(fitness_filepath, "w", stdout); */
  


  /* gsl_siman_solve(r, &x_initial, E2, S2, M2, P2, */
  /*                 NULL, NULL, NULL,  */
  /*                 sizeof(x_initial), params); */

  gsl_siman_solve(r, &x_initial, E4, S4, M4, P4,
                  NULL, NULL, NULL, 
                  sizeof(x_initial), params);

  return 0;
}
