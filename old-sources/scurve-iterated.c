/* initial solution */
/* local search */
/* search history = initial sol */
/* while n-iters */
/* scurve = perturbation (initial, search history) */
/* if fitness > fitness_initial */
/* scurrent = sbest */
/* end */


/* THIS USES HILL CLIMBING AS LOCAL SEARCH */

#include "scurve-types.h"
#include "prototypes.h"

Source_type source;
double step_size, noise_percentage;
Poly scurve_true, scurve_initial;
int n_steps, real_shift;
int nr_computations;

int main(int argc, char *argv[])
{
  char output_dir[MAX_FILENAME_LEN];
  char fitness_filepath[MAX_FILENAME_LEN], fitness_filepath_only_increases[MAX_FILENAME_LEN];
  double *y_readout = NULL; 
  char run_id_str_eps[MAX_FILENAME_LEN];
  setting_params(argc, argv, iterated);

  make_directories(output_dir); 
  if (is_finite(source)) {
    sprintf(run_id_str_eps, "eps%1.5g_srcY%1.5g_Z%1.5g",  
	    step_size, source.y, source.z);    
  } else {
    sprintf(run_id_str_eps, "eps%1.5g_srcA%1.5g",
	    step_size, source.a);
  }
  sprintf(fitness_filepath, "%s/%s_%s_%s", 
	  output_dir, "iterated", run_id_str_eps, "fit_timeline");
  sprintf(fitness_filepath_only_increases, "%s/%s_%s_%s", 
	  output_dir, "iterated", run_id_str_eps, "fit_timeline_incr");
  print_metadata(fitness_filepath);

  print_state_header(fitness_filepath);
  prepare_mask_and_readout(&y_readout, generate);
  add_noise(noise_percentage, y_readout);

  int step;
  double fitness_next;
  Poly scurve = scurve_initial;
  Poly scurve_next =  scurve_initial;
  double fitness = calc_fitness(scurve, y_readout);
  double entropy = shannon_entropy(iterated, scurve_initial, y_readout);


  for (step = 0; step < n_steps; ++step) {
    /* take a step */
    scurve_next = hill_take_step(scurve);
    scurve_next = perturb_iterated(scurve_next);

    fitness_next = calc_fitness(scurve_next, y_readout);
    /* entropy = shannon_entropy(iterated, scurve, fitness,  y_readout); */
    /* if fitness has improved, update position in search space */
    if (fitness_next > fitness) {
      fitness = fitness_next;
      scurve = scurve_next;
      scurve = update_c0(scurve, source, y_readout);

      print_state(fitness_filepath_only_increases, step, fitness, scurve, 
		  step_size, entropy,0);

      if(entropy_indicator){
	entropy = shannon_entropy(iterated, scurve, y_readout);
      /* print information if fitness improved or stayed the same */
      }
      /*else{
	print_state(fitness_filepath, step, fitness, scurve, step_size, 0);
	}*/
      print_fitness_diagnostics(output_dir, step, scurve, 
				y_readout, iterated, step_size);
    }
    print_state(fitness_filepath, step, fitness,scurve, step_size, entropy,0);

  }
  return 0;
}
