#include "scurve-types.h"
#include "prototypes.h"

void adapt_prep_step(Poly prev_poly, Poly *pnormal, Poly *plarge,
                     int step, double step_size_initial,
                     double *step_size_large)
{
  /* interesting note: the adaptive step uses hill-climbing-style
     steps under the hood */
  *pnormal = hill_take_step(prev_poly);
  if (step % large_step_period == 0) 
    {
      *step_size_large = step_size_initial * adapt_step_large_scaling;
    } 
  else 
    {
      *step_size_large = step_size_initial * adapt_step_small_scaling;
    }
  *plarge = hill_take_step(prev_poly);
}

void adapt_prep_step_2_plane(Poly prev_poly, Poly *pnormal, Poly *plarge,
                     int step, double step_size_initial,
                     double *step_size_large, int coeff_choice1, 
		     int coeff_choice2, 
		     double coeff_choice1_val, double coeff_choice2_val)
{
  /* interesting note: the adaptive step uses hill-climbing-style
     steps under the hood */
  *pnormal = hill_take_step_2_plane(prev_poly,coeff_choice1, coeff_choice2, 
		     coeff_choice1_val, coeff_choice2_val);
  if (step % large_step_period == 0) 
    {
      *step_size_large = step_size_initial * adapt_step_large_scaling;
    } 
  else 
    {
      *step_size_large = step_size_initial * adapt_step_small_scaling;
    }
  *plarge = hill_take_step_2_plane(prev_poly, coeff_choice1, coeff_choice2, 
		     coeff_choice1_val, coeff_choice2_val);
}
/*
Poly adapt_take_step(Poly prev_poly)
{
  return prev_poly;
}
*/
