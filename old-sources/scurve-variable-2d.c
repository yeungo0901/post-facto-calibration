#include "scurve-types.h"
#include "prototypes.h"


#define N_DIM 4
#define N_SLICES (N_DIM*(N_DIM-1)/2) 
/* FIXME: must find simpler formula for SLICE2IND() */
#define SLICE2IND(i, j) (i == 0 ? (j-1) : (i == 1 ? (j+1) : (j+2)))

Source_type source;
double step_size, noise_percentage;
Poly scurve_true, scurve_initial;
int n_steps, real_shift;
int entropy_indicator;

int main(int argc, char *argv[])
{
  char output_dir[MAX_FILENAME_LEN];
  /* char fitness_filepath[N_SLICES][MAX_FILENAME_LEN]; */
  double *y_readout = NULL; 
  char run_id_str_eps[MAX_FILENAME_LEN];
  setting_params(argc, argv, variable);
  make_directories(output_dir);
  prepare_mask_and_readout(&y_readout, generate);
  add_noise(noise_percentage, y_readout);

  FILE *fp_list[N_SLICES];
  char fname_list[N_SLICES][MAX_FILENAME_LEN];
 
  if (is_finite(source)) {
    sprintf(run_id_str_eps, "eps%1.5g_srcY%1.5g_Z%1.5g", 
	    step_size, source.y, source.z);    
  } else {
    sprintf(run_id_str_eps, "eps%1.5g_srcA%1.5g", step_size, 
	    source.a);
  }
  int i2,i1,j1, j2;     /* the coefficients that define the slice */
  int slice_ind;                /* the order index of the slice */
  for (i1 = 0; i1< N_DIM; ++i1) {
    for (j1 = i1+1; j1 < N_DIM; ++j1) {
      slice_ind = SLICE2IND(i1, j1);
      sprintf(fname_list[slice_ind],
              "%s/variable_%s_fit_land_plane_%d_%d", 
	      output_dir, run_id_str_eps, i1, j1);
      fp_list[slice_ind] = fopen(fname_list[slice_ind], "w");
      print_metadata(fname_list[slice_ind]);
    }
  }

  Poly scurve_list[N_SLICES], scurve_next_list[N_SLICES];  
  Poly fulcrum_poly = scurve_initial; 

  int step;
  double fitness_list[N_SLICES];
  double fitness_next_list[N_SLICES];
  int fpl;

  for (i2 = 0; i2 < N_DIM; ++i2) {
    for (j2 = i2+1; j2 < N_DIM; ++j2) {
      slice_ind = SLICE2IND(i2, j2);
      scurve_list[slice_ind] = fulcrum_poly; 
      double coeff_1_val, coeff_2_val; 
      assign_hyperplane(&coeff_2_val, &coeff_1_val, i2, j2);
      scurve_list[slice_ind] = hill_take_step_2_plane(scurve_list[slice_ind], 
						      i2, j2, coeff_1_val, 
						      coeff_2_val); 

      for(fpl=0;fpl<N_SLICES;fpl++)
	{
	  fitness_list[fpl] = calc_fitness(scurve_list[fpl], y_readout);
	}

      for (step = 0; step < n_steps; ++step) {
	scurve_next_list[slice_ind] = variable_take_step_2_plane(scurve_list
								 [slice_ind], 
								 y_readout, i2, 
								 j2,
								 coeff_1_val, 
								 coeff_2_val);
	fitness_next_list[slice_ind] = calc_fitness(scurve_list[slice_ind], 
						    y_readout);
	if (fitness_next_list[slice_ind] > fitness_list[slice_ind])
	  {
	    fitness_list[slice_ind] = fitness_next_list[slice_ind];
	    scurve_list[slice_ind] = scurve_next_list[slice_ind];
	    print_state(fname_list[slice_ind], step, 
			fitness_list[slice_ind], 
			scurve_list[slice_ind], step_size, 0, 0);
	    print_fitness_diagnostics(output_dir, step, scurve_list[slice_ind], 
				      y_readout, variable, step_size);
	  }
      }
    }
  }
  int n_slice_ind;
  for(n_slice_ind =0; n_slice_ind < N_SLICES; n_slice_ind++){
    fclose(fp_list[n_slice_ind]);
  }
  return 0;
}

