#ifdef _OPENMP
#include <omp.h>

#endif /* _OPENMP */
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <gsl/gsl_randist.h>

#include "scurve-types.h"
#include "prototypes.h"
#include "constants.h"
/** 
 * 
 * calculates the fitness of a given scurve.  this is done by applying
 * the scurve to the readout and seeing how cleanly that convolves
 * with the mask
 * 
 * @param scurve 
 * @param y_readout h
 * @param step 
 * @param output_dir 
 * @param do_diagnostics 
 * 
 * @return 
 */
double calc_fitness(Poly scurve, double *y_readout)
{
  nr_computations++;

  double *y_real_guess = malloc(n_det_bins*sizeof(*y_real_guess));
  scurve_poly(y_readout, y_real_guess, scurve);
  double *convolution = malloc(n_det_bins*sizeof(*convolution));
  fill_convolution(y_real_guess, convolution);
  conv_sig_max convo = metric_peak_ratio(convolution);
  double fitness = convo.sig;

  free(y_real_guess);
  free(convolution);

  return fitness;
}

/** 
 * one of the metrics we can use to see how good an s-curve is; this
 * one takes the convlution of the scurve with the mask and returns
 * the ratio of the highest peak to the next-highest peak.
 * 
 * @param convolution 
 * @return the ratio of the highest peak to the next-highest
 */
conv_sig_max metric_peak_ratio(double *convolution)
{
  
  conv_sig_max conv;

  int i,j, imax = 0 /*, i2nd=0 */;
  double max = convolution[0];
  double max2nd = -HUGE_VAL;
  double sum_conv = 0, sum_conv2 = 0; /* to find sigma */
  double mean_conv;
  double sum_conv_minus_mean_2 = 0;
  for (i = 0; i < n_det_bins; ++i) {
    /* first time through we calculate what we need for the mean */
    sum_conv += convolution[i];
    sum_conv2 += convolution[i]*convolution[i];
    if (convolution[i] > max) {
      max = convolution[i];
      imax = i;
    }
  }
  mean_conv = sum_conv / n_det_bins;
  for (j = 0; j < n_det_bins; ++j) {
    sum_conv_minus_mean_2
      += (convolution[j] - mean_conv) * (convolution[j] - mean_conv);
    if ((j != imax) && (convolution[j] > max2nd) && (convolution[j]!= max)) { /* skip the max */
      max2nd = convolution[j];
      /* i2nd = j; */
    }
  }
  double sigma = sqrt(sum_conv_minus_mean_2/n_det_bins);
  if (sigma == 0) {
    conv.sig = 0;
    conv.max = imax; /* FIXME: what should this be set to? */
  } 
  else{
    double significance = (max - mean_conv) / sigma;
    conv.sig = significance;
    conv.max = imax;
  }
  return conv; 


}

double rand_in_bounds(double min, double max){

  /*double a = -2.5;

  double b = 2.5;

  const gsl_rng_type * T;
  gsl_rng * r;

  gsl_rng_env_setup();
     
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  
  double random = gsl_ran_gumbel2(r, a, b);
  gsl_rng_free (r); */

  return min + ((max-min)*random())/RAND_MAX; 


}


void fill_convolution(double *input, double *result)
{
  int j,shift_ind;
  double y;
  memset(result, 0, n_det_bins*sizeof(*result));

#ifdef _OPENMP
  int nthreads, th_id;
  static int first_time = 1;

  /* loop through all possible shifts and all detector pixels */
#define CHUNKSIZE 1
#pragma omp parallel private(j, y)
  {
#pragma omp barrier
    if ( first_time && th_id == 0 ) {
      first_time = 0;
      nthreads = omp_get_num_threads();
      printf("There are %d threads\n",nthreads);
    }
#pragma omp for schedule(dynamic, CHUNKSIZE)
#endif /* _OPENMP */
  for(shift_ind=0; shift_ind<n_det_bins; shift_ind++){
    
    for (j = 0; j < n_det_bins; ++j) {
      /* calculate corresponding mask pixel */

      y = (1.0*((j+shift_ind)%n_det_bins)/(1.0*n_det_bins))*detector_height;
      /* assert(y >= 0 && y<detector_height); */
      /* compute convolution of guess scurve and mask */
      result[shift_ind]+= input[j]*open_mask(y);
    }
  }
#ifdef _OPENMP
  }
#endif /* _OPENMP */

}

