#! /usr/bin/env python

import sys
import glob
import os
from pprint import pprint
import string
import getopt
import re

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D, axes3d
from matplotlib.mlab import griddata
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from matplotlib import rc
import numpy as np
import mpl_toolkits.mplot3d.axes3d as p3
 
markers = ('^', 'o', 'p', 's', '8', 'h', 'x', 'v')
colormap = cm.jet
correlation_style = 'same'              # 'full' or 'same'
#ncolormap = cm.bwr
#colormap = cm.seismic

global_fit_max = sys.float_info.min # initial values before we find them
global_fit_min = sys.float_info.max

def __MAIN__():

  interactive = False
  (opts, args) = getopt.getopt(sys.argv[1:], 'i', [])
  for o, a in opts:
    if o == '-i':                       # get a single date
      interactive = True
  if args:
    outdir_list = args
  else:
    outdir_list = glob.glob('outdir_*')
  outdir_list.sort()

  pprint('# processing this list of output directories:')
  pprint(outdir_list)
  ## now the actual work of generating the plots
  results_dict = {}
  for outdir in outdir_list:
    plot_fit_landscapes_trajectories(outdir)

  ## now a little feature: if the command line says "interactive" 
  if interactive:
    plt.show()

def plot_fit_landscapes_trajectories(outdir):

  ## find all landscape files 
  fnames_landscape = glob.glob(outdir + '/fit_landscape_plane*')
  print fnames_landscape

  for fl in fnames_landscape:
    print fl
    run_id_start_pos = string.find(fl, 'fit_landscape_')
    run_id_land = fl[run_id_start_pos + len('fit_landscape_') : ]
    slice_compile = re.compile('.*plane_(.*?)src.*')
    slice_search = slice_compile.search(str(run_id_land))

    if not slice_search:
      print 'dude, pattern not found'
      print 'plane_(.*?)src'
      print run_id_land
    slice_current = slice_search.group(1)   
    slice_first_compile = re.compile('(.*?)_')
    slice_first = slice_first_compile.search(slice_current)
    if slice_first:
      coeff_land_1 = slice_first.group(1)
    slice_second_compile = re.compile('_(.*?)_')
    slice_second = slice_second_compile.search(slice_current)
    coeff_land_2 = slice_second.group(1)

    xl = {}
    yl = {}
    zl = {}
    xi = {}
    yi = {}
    X = {}
    Y = {}
    Z = {}
    ax = {}
    fig = {}
    surf = {}

    ## extract data from file that i need
    data_landscape = {}
    data_landscape[fl] = np.genfromtxt(fl)
    xl[fl] = data_landscape[fl][:,int(coeff_land_1)]
    if len(xl[fl]) == 0:
      print 'file %s not found; continuing' % fl
      continue

    yl[fl] = data_landscape[fl][:,int(coeff_land_2)]
    zl[fl] = data_landscape[fl][:,4]
      
    xi[fl] = np.linspace(min(xl[fl]),max(xl[fl]))
    yi[fl] = np.linspace(min(yl[fl]),max(yl[fl]))     
    X[fl],Y[fl] = np.meshgrid(xi[fl], yi[fl])
    Z[fl] = griddata(xl[fl],yl[fl],zl[fl],xi[fl],yi[fl])
    fig[fl] = plt.figure()
    ax[fl] = fig[fl].add_subplot(111, projection='3d')
 
    #surf[fl] = ax[fl].plot_surface(X[fl], Y[fl], Z[fl], rstride=5, cstride=5, 
     #                              cmap=cm.jet, linewidth=1, antialiased=True)
    surf[fl] = ax[fl].plot_surface(X[fl], Y[fl], Z[fl], cstride=5, 
                                   cmap=cm.jet, alpha = 0.1)
    fnames = glob.glob(outdir + '/*fit_land_plane_%s_%s*' % (str(coeff_land_1), str(coeff_land_2)))
    ## now look at landscape slice files for different algorithms in the same hyperplane

    for timeline_file in fnames:
      ax[fl].azim = -164
      ax[fl].elev = 59
      print timeline_file
      data_timeline = {}
      xt = {}
      yt = {}
      Xt = {}
      xti = {}
      yti = {}
      Yt = {}
      Xt = {}
      fitt = {}
      Fitt = {}
      data_timeline[timeline_file] = np.genfromtxt(timeline_file) 
      xt[timeline_file] = data_timeline[timeline_file][:,int(coeff_land_1)+4]
      if len(xt) == 0:
        print 'file %s not found; continuing' % timeline_file
        continue  
      yt[timeline_file] = data_timeline[timeline_file][:,int(coeff_land_2)+4]
      fitt[timeline_file] = data_timeline[timeline_file][:,1]   

      ax[fl].scatter(xt[timeline_file], yt[timeline_file],
                 zs=fitt[timeline_file], c='green', alpha=1.0,
                 label='search points')
      ax[fl].plot(xt[timeline_file], yt[timeline_file],
                 zs=fitt[timeline_file], c='red', alpha=1.2, label='search path')


    fname = outdir + '/' + 'trajectory_land_sweep_%s.pdf' % (run_id_land)

    plt.savefig(fname, format='pdf')
    plt.close()
    prepare_legend(plt)
    print 'saved %s' % fname

def prepare_legend(plt, loc='upper left'):
  """prepare the legend; for our plots we make it somewhat transparent
  and we put it in the upper left corner and we choose a small font
  """
  plt.legend(loc=loc, shadow=True, ncol=1, prop={'size':6})
  #plt.bar(label = 'fitnesses and trajectories') 
  plt.rc('font',**{'family':'serif','serif':['Computer Modern Roman'],'size':12})
  leg = plt.gca().get_legend()
  #ltext = leg.get_texts()
  #leg.get_frame().set_alpha(0.2)

if __name__ == '__main__':
  __MAIN__()
