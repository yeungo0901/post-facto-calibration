====================
 Background reading
====================

Some hasty notes on resources:

All the papers in the ``papers`` directory at the top level of this
repository.  These have not yet been sorted or carefully curated.

https://en.wikipedia.org/wiki/Proportional_counter

S-curve in agriculture:
https://en.wikipedia.org/wiki/Van_Genuchten%E2%80%93Gupta_model
has interesting functional forms for s-curves, although they are
flipped, which can probalby be fixed.
