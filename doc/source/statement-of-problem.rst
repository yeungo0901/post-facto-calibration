==========================
 Statement of the problem
==========================

Background story
================

Let's say you have built an instrument which measures something.

Not very precise, eh?  Let me give two examples:

A camera
   Many consumer cameras today (in 2022) are based on charge coupled
   devices (CCDs).  The CCD has many pixels, and they form an image.
   CCDs can be used for many different photon energies, but for our
   purposes let us for now think "CCD <-> visible light cameras" (like
   your cell phone).

A proportional counter wire with a coded mask
   Proportional counters detect *ionizing radiation* (for example,
   photons in the X-ray spectrum).  A proportional counter will give
   you an "approximate location along a wire (the "anode wire") where
   the photon hit" (more precisely: "an approximate position near
   which the photon inoized the gas").  The mask then allows you to
   detect the shadow of a beam of X-rays.

We will discuss both of these kinds of detectors at greater length,
but for now I will simply mention that in both types of detectors have
to be *calibrated* to account for an imperfect correspondence between
what they report, and what actually happened.

The distortions that can happen in both of these detectors are quite
different in nature: the CCD could have bad pixels, regions that
record intensity or color incorrectly, or even aberration in the
optical lens.  Proportional counters can have imperfections in how the
anode wire attracts the electrons freed by the ionizing X-ray, and
they can suffer from slow loss of the gas in the chamber.

Of all these errors (color, photon energy, position, ...), let us
focus now on the *position* error in the proportional counter.

Referring to the diagrams in the `Dept. of Energy Instrumentation and
Control Handbook
<https://www.nuclear-power.com/nuclear-engineering/radiation-detection/gaseous-ionization-detector/proportional-counter-proportional-detector/>`_
and the `subsequent page
<https://www.nuclear-power.com/nuclear-engineering/radiation-detection/gaseous-ionization-detector/proportional-counter-proportional-detector/advantages-and-disadvantages-of-proportional-counters/>`_:

The wire position of each photon can be obtained from the pulse of
charges that accumulate the wire - that is the fulcrum of the
technique.  Ideally you would like to have a straight line plot of
*real position* versus *pulse measurement on the wire*.

But that is not what happens: a variety of physical effects make it a
non-linear relationship.  When the instrument is built, a careful
procedure (sometimes involving shining photons onto each bit of the
wire) lets us calculate the calibration correction.  This currection
turns the straight line that maps reality to measurement becomes a
curve, sometimes called an "s-curve" because it has an approximately
sigmoid shape.

Everything works out well: we get good positions.

You can think of the s-curve correction as a mapping:

"naive calculation of position" -> "realistic calcuation of position"

But... in many detectors the physical situation changes over time, and
the s-curve changes over time.


Curve fitting
=============

A final thing to mention in this introduction: the s-curve ends up
looking like a curve fit.  If :math:`x` is the naive pulse location
and :math:`P_{\rm calibrated}` is the real position, then we fit a function
to the calibration data and get something like:

.. math::

   P_{\rm calibrated}(x) = \frac{a_0}{x - a_1} + a_2 + a_3 x

In this functional form, x appears in :math:`a_3 x`, which would have
given us the nice linear relationship.  But it also appears in
:math:`a_0 / (x - a_1)`, which gives it the s-curve shape.

The parameters :math:`a_0, a_1, a_2` come from a *curve fitting* that
used the calibration measurements.


The question
============

It can be impossible or very difficult to re-calibrate the instrument
after it has been deployed.  This is even more true for space-based
instrumention.

And an instrument's performance will degrade or change with time.

So our question is:

   **how do you update the s-curve when you cannot redo the calibration?**

This is sometimes called *post-facto calibration*, and we will propose
a technique.


The technique
=============

.. sidebar:: A playful analogy

   A playful analogy to this technique might be:

   You are trying to cook a pasta sauce in the best possible way --
   let us say "ravioli con panna, prosciutto, piselli, e funghi".  You
   are not sure of the best order in which to add ingredients to the
   stir-fry.  Do you first saute the onions and then add the
   prosciutto?  Or do the peas before everything else?  At what time
   should you add the heavy cream?

   You wish you had asked the person who first showed you this dish,
   long ago on a trip to Italy, for the recipe, but you didn't think
   you would ever cook it yourself.

   So what do you do?

   One approach might be be to define a metric for the best pasta
   sauce: measure how many people at your dinner party go for seconds.

   Then every week you host another dinner party in which you cook this
   meal adding the ingredients in a different order, and keep track of
   the metric for that evening.

   Once you have tried all possible procedures for cooking, you know the
   order which maximizes the metric.  And you did this based on a
   metric that did not require going back to the original source of
   the recipe.

Without a direct measurement that lets you optimize the :math:`{a_i}`
parameters with a curve fitting procedure, we need a different
approach.

The proposal here is to use overall image "sharpness" as a measurement
of how effective the s-curve correction is.

How do we define "sharpness"?  That is one of the main research
topics in this project.

It has to be a function of the image numbers (which in turn are
corrected by our s-curve), and the function has to return a single real
number - the *quality* of the image.  We will call this the *metric*.

This will allow us to search through the space of *all possible
s-curve functions*, looking for the functional form and coefficients
that yield the "best" image -- the image with the highest metric.

This approach of using a metric intrinsic to the instrument's
operation allows you to calibrate without having to dismantle and
rebuild the instrument.
