.. Post-Facto Calibration documentation master file, created by
   sphinx-quickstart on Sun Jul 10 02:56:26 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Post-Facto Calibration
======================

   :Date: |today|
   :Author:  **Christina Go** <yeungo0901@gmail.com>
   :Author:  **Michael Bengil** <myk0675@gmail.com>
   :Author:  **Mark Galassi** <mark@galassi.org>
   :Author:  **Ed Fenimore** <efenimore@lanl.gov>
   :License: This is a free book.  You may adapt and redistribute it
             under the terms of the Creative Commons
             Attribution-ShareAlike 4.0 International license (CC
             BY-SA 4.0) described at
             https://creativecommons.org/licenses/by-sa/4.0/

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   statement-of-problem.rst
   challenges-and-discovery.rst
   prongs-to-start-the-research.rst
   background-reading.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
