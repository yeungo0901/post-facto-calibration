==========================
 Challenges and discovery
==========================

There are some challenges in carrying out this approach.  There are
also some very cool things that might come out of it.  At this time
(2022-07-10) I (markgalassi) can predict the following:


Creating the sharpness metric
=============================

This might be the most algorithmically interesting aspect of the
project.

There are some ideas I can provide when we get to it.  Here are
placeholder names:

In one-dimension one can use "ratio of highest two peaks in
convolution", "maximum number of open slits for each shift alignment".

Note that in the case of coded apertures, the metric might also be
used as a tool in the design of the mask and reconstruction algorithm.

In two dimensions there probably exist standard "is it in focus?"
algorithms.  Two that come to mind are contrast-to-noise ratio (CNR),
and signal-to-noise-ratio (SNR).  The two can be combined.  Another
thing to look up is the FDR (False Discovery Rate).


Modeling instrument distortion mathematically
=============================================

Finding a model for a type of instrument, such as a coded aperture
imager, or an optical CCD.

This model should be complex enough that it allows distortion of the
measurements, but also simple enough that we can control how the
distortion happens.


Finding real instrument data
============================

We might be able to find the HETE-2 satellite data.  We might also be
able to find data sets used to calibrate optical CCDs, either for
telescopes in astronomical projects or for consumer cameras and cell
phones.


Developing the search approach
==============================

Finding a way to search through all possible parametrizations of a
function.  Techniques to try range from simple hand-coded
hill-climbing, to genetic algorithms, to simulated annealing and its
variants.


Fitness landscape and entropy
=============================

Searches through rich parameter spaces can make for interesting
studies of the fitness landscape associated with that problem.
Visualizing high-dimensional fitness landscapes is an intriguing
challenge.

In addition, the fitness landscape has a unique way of transferring
information onto the state of the search.  The study of phase
transitions and entropy in the search, as shown by Chris Adami, is
interesting and ripe for discovery.

Search problems that are complex enough can give insight into this.
